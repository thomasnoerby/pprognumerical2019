#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
void two_step(
	double h,
	double tim,
	double ti,
	gsl_vector* ytm, 
	gsl_vector* yt, 
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* dydt,
	gsl_vector* err
);

void rkstep45(
	double t,
	double h,
	gsl_vector* yt,
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* yth,
	gsl_vector* err
);

int driver(
	double b,
	double* h,
	double acc,
	double eps,
	void one_stepper(
		double t, double h, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err
		),
	void two_stepper(
		double h, double tim, double ti,
		gsl_vector* ytm, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* dydt, gsl_vector* err
	),
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* t_steps,
	gsl_matrix* y_steps,
	int max
);
double norm(gsl_vector* x);
