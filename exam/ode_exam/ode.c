#include"ode.h"
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<assert.h>
#include<stdio.h>

void two_step(
	double h,
	double tim, // t[i-1]
	double ti, // t[i]
	gsl_vector* ytm, // y[i-1]
	gsl_vector* yt, // y[i]
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* dydt,
	gsl_vector* err)
{
	int n = yt->size;
	
	f(ti,yt,dydt);

	double c_val, ytm_i, yt_i, dydt_i, diff;
	for(int i = 0; i<n; i++) {
		ytm_i = gsl_vector_get(ytm,i);
		yt_i = gsl_vector_get(yt,i);
		dydt_i = gsl_vector_get(dydt,i);	
		diff = ti-tim;

		c_val = (ytm_i-yt_i+dydt_i*diff)/diff/diff;
		gsl_vector_set(err,i,c_val*h*h);
		
		gsl_vector_set(yt,i,yt_i+dydt_i*h+c_val*h*h);
	}	
}

void rkstep45(
	double t,
	double h,
	gsl_vector* yt,
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* yth,
	gsl_vector* err)
{
	int n = yt->size;
	gsl_vector* k0 = gsl_vector_alloc(n);	
	gsl_vector* k1 = gsl_vector_alloc(n);	
	gsl_vector* k2 = gsl_vector_alloc(n);	
	gsl_vector* k3 = gsl_vector_alloc(n);	
	gsl_vector* k4 = gsl_vector_alloc(n);	
	gsl_vector* k5 = gsl_vector_alloc(n);	
	gsl_vector* yx = gsl_vector_alloc(n);

	f(t,yt,k0);
	int i;
	double val, val_err, yti, k0i, k1i, k2i, k3i, k4i, k5i;
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		val = yti+1.0/4*k0i*h;	
		gsl_vector_set(yx,i,val);
	}

	f(t+1.0/4*h,yx,k1);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		val = yti+(3.0/32*k0i + 9.0/32*k1i)*h; 
		gsl_vector_set(yx,i,val);
	}

	f(t+3.0/8*h,yx,k2);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		k2i = gsl_vector_get(k2,i);
		val = yti+(1932.0/2197*k0i - 7200.0/2197*k1i + 7296.0/2197*k2i)*h; 
		gsl_vector_set(yx,i,val);
	}
	
	f(t+12.0/13*h,yx,k3);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		k2i = gsl_vector_get(k2,i);
		k3i = gsl_vector_get(k3,i);
		val = yti+(439.0/216*k0i - 8.0*k1i + 3680/513*k2i - 845.0/4104*k3i)*h; 
		gsl_vector_set(yx,i,val);
	}
	
	f(t+h,yx,k4);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		k2i = gsl_vector_get(k2,i);
		k3i = gsl_vector_get(k3,i);
		k4i = gsl_vector_get(k4,i);
		val = yti+(-8.0/27*k0i + 2*k1i - 3544.0/2565*k2i + 1859/4104*k3i - 11.0/40*k4i)*h; 
		gsl_vector_set(yx,i,val);
	}

	f(t+1.0/2*h,yx,k5);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		k2i = gsl_vector_get(k2,i);
		k3i = gsl_vector_get(k3,i);
		k4i = gsl_vector_get(k4,i);
		k5i = gsl_vector_get(k5,i);
		val = yti+(16.0/135*k0i + 6656.0/12825*k2i + 28561.0/56430*k3i -9.0/50*k4i + 2.0/55*k5i)*h;	
		gsl_vector_set(yth,i,val); // The best guess of the next function value 
		val_err = yti + (25.0/216*k0i + 1408.0/2565*k2i + 2197.0/4104*k3i - 1.0/5*k4i)*h;
		gsl_vector_set(err,i,val-val_err); // The error on the guess
	}

	gsl_vector_free(k0);
	gsl_vector_free(k1);
	gsl_vector_free(k2);
	gsl_vector_free(k3);
	gsl_vector_free(k4);
	gsl_vector_free(k5);
	gsl_vector_free(yx);
}

int driver(
	double b,
	double* h,
	double acc,
	double eps,
	void one_stepper(
		double t, double h, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err
		),
	void two_stepper(
		double h, double tim, double ti, 
		gsl_vector* ytm, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* dydt, gsl_vector* err
	),
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* t_steps,
	gsl_matrix* y_steps,
	int max)
{
	int n = y_steps->size2;
	gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* yt = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	gsl_vector* dydt = gsl_vector_alloc(n);

	double power = 0.25;
	double safety = 0.95;
	double a = gsl_vector_get(t_steps,0);
	double tau_i, e_i;
	double scale;

	int iter = 0;
	int maxiter = max*10;
	int i = 0;
	double t = a;
	double y_steps_imj, y_steps_ij;
	while(i < 2 && iter<maxiter) {
		iter++;
		if(t+*h>b) {
			*h = b-t;
		}
		
		for(int j = 0; j<n; j++) {
			y_steps_ij = gsl_matrix_get(y_steps,i,j);
			gsl_vector_set(yt,j,y_steps_ij);		
		}
		one_stepper(t,*h,yt,f,yth,err); 

		tau_i = (eps * norm(yth) + acc) * sqrt(*h / (b-a));
		e_i = norm(err);

		if(e_i < tau_i) {
			i++;
			t += *h;	
			gsl_vector_set(t_steps,i,t);
			gsl_matrix_set_row(y_steps,i,yth);
		}

		if(e_i > 0) {
			scale = pow(tau_i/e_i,power)*safety;
			*h *= scale;		
		}
	}

	assert(i == 2);

	double tim = gsl_vector_get(t_steps,i-1);
	double ti = gsl_vector_get(t_steps,i);

	while(t < b && i<max && iter<maxiter) {
		iter++;

		if(t+*h>b) {
			*h = b-t;
		}

		two_stepper(*h,tim,ti,yt,yth,f,dydt,err);	
		tau_i = (eps * norm(yth) + acc) * sqrt(*h / (b-a));
		e_i = norm(err);

		if(e_i < tau_i) {
			i++;
			t += *h;
			for(int j = 0; j<n; j++) {
				y_steps_imj = gsl_matrix_get(y_steps,i-1,j);
				gsl_vector_set(yt,j,y_steps_imj);
			}

			gsl_vector_set(t_steps,i,t);
			gsl_matrix_set_row(y_steps,i,yth);

			tim = ti;
			ti = t;
		}
		else {
			for(int j = 0; j<n; j++) {
				y_steps_imj = gsl_matrix_get(y_steps,i,j);
				gsl_vector_set(yth,j,y_steps_imj);
			}
		}

		if(e_i > 0) {
			scale = pow(tau_i/e_i,power)*safety;
			*h *= scale;
		}
	}
	fprintf(stderr,"Driver finished after %d iterations and %d steps\n",iter,i);
	gsl_vector_free(yth);
	gsl_vector_free(yt);
	gsl_vector_free(err);
	gsl_vector_free(dydt);
	return i+1;
}

double norm(gsl_vector* x)
{
	int n = x->size;
	double length = 0;
	for(int i = 0; i<n; i++) {
		double xi = gsl_vector_get(x,i);
		length += xi*xi; 	
	}
	length = sqrt(length);
	
	return length;
}
