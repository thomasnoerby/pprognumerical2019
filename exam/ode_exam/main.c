#include"ode.h"
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"gen_rel_orbit.h"
#include<stdio.h>
#include<math.h>
void ode(double t, gsl_vector* y, gsl_vector* dydt);
void gen_rel_ode_orbit(double t, gsl_vector* y, gsl_vector* dydt);
void ode_sin(double t, gsl_vector* y, gsl_vector* dydt);

int main() {
	fprintf(stderr,"Solving ode system with relativistic orbit:\n");
	fprintf(stderr,"y0' = y1, y1' = 1 - y0 + correction * y0 * y0\n");
	int n = 2;
	int max = 20000;
	gsl_vector* t_steps = gsl_vector_alloc(max);
	gsl_matrix* y_steps = gsl_matrix_alloc(max,n);

	double t = 0;
	double h = 0.05;
	double b = 10.0*M_PI;
	
	double y0 = 1;
	double y1 = -0.5;

	gsl_vector_set(t_steps, 0, t);
	gsl_matrix_set(y_steps, 0, 0, y0);
	gsl_matrix_set(y_steps, 0, 1, y1);
	fprintf(stderr,"Startpoint a = %g, endpoint b = %g, initial stepsize h = %g\n",t,b,h);
	fprintf(stderr,"Inital conditions are y0 = %g and y1 = %g\n",y0,y1);
	fprintf(stderr,"Correction = 0.01 has been chosen\n");

 
	double abs = 1e-3, eps = 1e-3;
	fprintf(stderr,"Running driver on system\n");
	int no_of_steps = driver(b,&h, abs, eps, rkstep45, two_step, gen_rel_ode_orbit, t_steps, y_steps, max);

	double y_val_0, t_val;
	for(int i = 0; i<no_of_steps; i++) {
		y_val_0 = gsl_matrix_get(y_steps,i,0);	
		t_val = gsl_vector_get(t_steps,i);
		printf("%g %g\n",t_val,y_val_0);
	}


	fprintf(stderr,"\nSolving the same ode with gsl\n");
	
	FILE* fp = fopen("gsl_data.txt","w+");
	double result;
	int iter = 0;
	for(double x = 0; x<b; x+=0.1) {
		result = gen_rel_orbit_gsl(x,0.01,y1, &iter);
		fprintf(fp,"%g %g\n", x, result);
	}
	fclose(fp);
	fprintf(stderr,"The gsl driver calculated the last point after %d iterations\n",iter);
	fprintf(stderr,"A comparison between the two step ode solver and gsl can be seen in the report\n");

	
	fprintf(stderr,"\nSolving simple sine ode system\n");
	fprintf(stderr,"y0' = y1, y1' = -y0\n");
	
	gsl_vector_set_zero(t_steps);
	gsl_matrix_set_zero(y_steps);

	h = 0.05;
	b = 3*M_PI;

	y0 = 0;
	y1 = 1;

	gsl_vector_set(t_steps, 0, t);
	gsl_matrix_set(y_steps, 0, 0, y0);
	gsl_matrix_set(y_steps, 0, 1, y1);
	fprintf(stderr,"Startpoint a = %g, endpoint b = %g, initial stepsize h = %g\n",t,b,h);
	fprintf(stderr,"Inital conditions are y0 = %g and y1 = %g\n",y0,y1);

	fprintf(stderr,"Running driver on system\n");
	no_of_steps = driver(b,&h, abs, eps, rkstep45, two_step, ode_sin, t_steps, y_steps, max);

	fp = fopen("sin_data.txt","w+");
	for(int i = 0; i<no_of_steps; i++) {
		y_val_0 = gsl_matrix_get(y_steps,i,0);	
		t_val = gsl_vector_get(t_steps,i);
		fprintf(fp,"%g %g\n",t_val,y_val_0);
	}
	fprintf(stderr,"The result can be seen in the report\n");
	fclose(fp);

	gsl_vector_free(t_steps);
	gsl_matrix_free(y_steps);
	return 0;
}

void ode_sin(double t, gsl_vector* y, gsl_vector* dydt)
{
	double y0 = gsl_vector_get(y,0);
	double y1 = gsl_vector_get(y,1);

	gsl_vector_set(dydt,0,y1);
	gsl_vector_set(dydt,1,-y0);
}

void gen_rel_ode_orbit(double t, gsl_vector* y, gsl_vector* dydt)
{
	double correction = 0.01;
	double y0 = gsl_vector_get(y,0);
	double y1 = gsl_vector_get(y,1);

	double dydt_val = 1-y0+correction*y0*y0;

	gsl_vector_set(dydt,0,y1);
	gsl_vector_set(dydt,1,dydt_val);
}
