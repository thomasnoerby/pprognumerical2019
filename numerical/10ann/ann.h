#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
typedef struct {int n; double (*f)(double); gsl_vector* data;} ann;
double activation_function(double x);
double activation_derivative(double x);
double activation_anti_derivative(double x);
ann* ann_alloc(int number_of_hidden_neurons, double (*activation_function)(double));
void ann_free(ann* network);
double ann_feed_forward(ann* network, double x);
double ann_feed_forward_der(ann* network, double x);
double ann_feed_forward_anti_der(ann* network, double x, double a);
void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist);
void print_params(gsl_vector* vec);
