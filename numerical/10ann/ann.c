#include"ann.h"
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include<math.h>
#include<assert.h>

double activation_function(double x) 
{
	double func = x*exp(-x*x); 
	return func;
}

double activation_derivative(double x)
{
	double der = exp(-x*x)*(1-2*x*x);
	return der;
}

double activation_anti_derivative(double x)
{
	double anti_der = -exp(-x*x)/2;
	return anti_der;
}

ann* ann_alloc(int number_of_hidden_neurons, double (*activation_function)(double))
{
	ann* network = (ann*) malloc(sizeof(ann));
	network->n = number_of_hidden_neurons;
	network->f = activation_function;
	network->data = gsl_vector_alloc(number_of_hidden_neurons*3);
	return network;
}

void ann_free(ann* network)
{
	gsl_vector_free(network->data);
	free(network);
}

double ann_feed_forward(ann* network, double x)
{
	int n = network->n;
	gsl_vector* data = network->data;
	double F = 0, a, b, w;
	double (*f)(double) = network->f;
	for(int i = 0; i<n; i++) {
		a = gsl_vector_get(data,i);
		b = gsl_vector_get(data,i+n);
		assert(b != 0);
		w = gsl_vector_get(data,i+2*n);
		F += f((x-a)/b)*w;
	}

	return F;
}

double ann_feed_forward_der(ann* network, double x)
{
	int n = network->n;
	gsl_vector* data = network->data;
	double F = 0, a, b, w;
	double (*f)(double) = activation_derivative;
	for(int i = 0; i<n; i++) {
		a = gsl_vector_get(data,i);
		b = gsl_vector_get(data,i+n);
		assert(b != 0);
		w = gsl_vector_get(data,i+2*n);
		F += f((x-a)/b)/b*w;
	}

	return F;
}

double ann_feed_forward_anti_der(ann* network, double x, double start)
{
	int n = network->n;
	gsl_vector* data = network->data;
	double F = 0, a, b, w;
	double (*f)(double) = activation_anti_derivative;
	for(int i = 0; i<n; i++) {
		a = gsl_vector_get(data,i);
		b = gsl_vector_get(data,i+n);
		assert(b != 0);
		w = gsl_vector_get(data,i+2*n);
		F += b * w * (f((a-x)/b)-f((a-start)/b));
	}

	return F;
}

void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist)
{
	int N = xlist->size;
	double fun(const gsl_vector* p, void* params)
	{
		gsl_vector_memcpy(network->data,p);
		double error = 0, feed, diff, x, y;
		for(int i = 0; i<N; i++) {
			x = gsl_vector_get(xlist,i);
			y = gsl_vector_get(ylist,i);
			feed = ann_feed_forward(network,x);
			diff = feed-y;
			error += diff*diff;
		}
		return error;
	}

	int n = network->n;
	int dim = n*3;
	gsl_vector* step = gsl_vector_alloc(dim);
	gsl_vector_set_all(step,0.01);

	gsl_multimin_fminimizer* fmin;
	fmin = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, dim);
	gsl_multimin_function fmin_fun;

	fmin_fun.f = fun;
	fmin_fun.n = dim;	
	fmin_fun.params = NULL;

	gsl_multimin_fminimizer_set(fmin, &fmin_fun, network->data, step);
	double size;
	double abs = 1e-6;
	int iter = 0;
	int max_iter = 1000000;
	int status;
	do {
		iter++;
		gsl_multimin_fminimizer_iterate(fmin);
		size = gsl_multimin_fminimizer_size(fmin);
		status = gsl_multimin_test_size(size, abs);
	}
	while (status == GSL_CONTINUE && iter<max_iter);
	fprintf(stderr,"Training completed after %d iterations\n",iter);
	
	gsl_multimin_fminimizer_free(fmin);
	gsl_vector_free(step);
}

void print_params(gsl_vector* vec) 
{
	int n = vec->size;
	for(int i = 0; i<n; i++) {
		if(i<n/3) {
			fprintf(stderr,"a_%d %6.3g\n",i,gsl_vector_get(vec,i));
		}
		if(i>=n/3 && i<2*n/3) {
			fprintf(stderr,"b_%d %6.3g\n",i%3,gsl_vector_get(vec,i));
		}
		if(i>=2*n/3) {
			fprintf(stderr,"w_%d %6.3g\n",i%3,gsl_vector_get(vec,i));
		}
	}

}
