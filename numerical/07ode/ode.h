#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
void rkstep45(
	double t,
	double h,
	gsl_vector* yt,
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* yth,
	gsl_vector* err
);

int driver(
	double b,
	double* h,
	gsl_vector* yt,
	double acc,
	double eps,
	void stepper(
		double t, double h, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err
		),
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* t_steps,
	gsl_matrix* y_steps,
	int max
);
double norm(gsl_vector* x);
void printer_vec(gsl_vector* vec);
void printer(gsl_matrix* matter);
