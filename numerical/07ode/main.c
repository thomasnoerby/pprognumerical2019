#include"ode.h"
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<stdio.h>
#include<math.h>
void ode(double t, gsl_vector* y, gsl_vector* dydt);
void gen_rel_ode_orbit(double t, gsl_vector* y, gsl_vector* dydt);

int main() {
	fprintf(stderr,"Solving orbit with relativistic precession\n");
	fprintf(stderr,"The equation is y'(t) = 1-y(t)+correction*y(t)*y(t) with a correction of -0.1\n");
	fprintf(stderr,"Running driver with rkf45\n");
	int n = 2;
	int max = 1000;
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* t_steps = gsl_vector_alloc(max);
	gsl_matrix* y_steps = gsl_matrix_alloc(max,n);

	double t = 0.0;
	double h = 0.05;
	double b = 10.0*M_PI;

	gsl_vector_set(t_steps, 0,t);
	gsl_matrix_set(y_steps,0,0,0.0);
	gsl_matrix_set(y_steps,0,1,0.5);

	
	double abs = 1e-2, eps = 1e-2;
	int no_of_steps = driver(b,&h,y,abs,eps,rkstep45,gen_rel_ode_orbit,t_steps,y_steps,max);
	fprintf(stderr,"Finished routine after %d number of steps\n",no_of_steps);
	fprintf(stderr,"The solution can be seen in gen-orbit.svg\n");
	
	double t_val, y_val_0, y_val_1;
	for(int i = 0; i<no_of_steps; i++) {
		t_val = gsl_vector_get(t_steps,i);
		y_val_0 = gsl_matrix_get(y_steps,i,0);
		y_val_1 = gsl_matrix_get(y_steps,i,1);
		printf("%g %g %g\n",t_val, y_val_0, y_val_1);
	}

	gsl_vector_free(y);
	gsl_vector_free(t_steps);
	gsl_matrix_free(y_steps);

	fprintf(stderr,"\nSolving simple differential equation\n");
	fprintf(stderr,"The equation is y'(t) = y(t)*(1-y(t))\n");
	fprintf(stderr,"Running driver with rkf45\n");

	h = 0.1;
	abs = 1e-6, eps = 1e-6;
	t = 0.0;
	b = 2*M_PI;
	n = 1;
	max = 1000;
	double ode_start = 0.5;

	y = gsl_vector_alloc(n);	
	t_steps = gsl_vector_alloc(max);
	y_steps = gsl_matrix_alloc(max,n);

	gsl_matrix_set(y_steps,0,0,ode_start);
	gsl_vector_set(t_steps,0,t);

	no_of_steps = driver(b, &h, y, abs, eps, rkstep45, ode, t_steps, y_steps, max);

	fprintf(stderr,"Finished routine after %d number of steps\n",no_of_steps);
	fprintf(stderr,"The solution can be seen in simple.svg\n");
	fprintf(stderr,"This matches exactly with the one from pprog/08orbit/orbit.svg\n");

	FILE* fp = fopen("simple_data.txt","w+");
	double y_val;
	for(int i = 0; i<no_of_steps; i++) {
		t_val = gsl_vector_get(t_steps,i);
		y_val = gsl_matrix_get(y_steps,i,0);
		fprintf(fp,"%g %g\n",t_val, y_val); 
	}
	fclose(fp);

	gsl_vector_free(y);
	gsl_vector_free(t_steps);
	gsl_matrix_free(y_steps);

	return 0;
}

void ode(double t, gsl_vector* y, gsl_vector* dydt)
{
	double y0 = gsl_vector_get(y,0);
	gsl_vector_set(dydt,0,y0*(1-y0));
}

void gen_rel_ode_orbit(double t, gsl_vector* y, gsl_vector* dydt)
{
	double correction = -0.1;
	double y0 = gsl_vector_get(y,0);
	double y1 = gsl_vector_get(y,1);

	double dydt_val = 1-y0+correction*y0*y0;

	gsl_vector_set(dydt,0,y1);
	gsl_vector_set(dydt,1,dydt_val);

}
