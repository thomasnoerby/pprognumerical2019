#include"ode.h"
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<assert.h>
#include<stdio.h>
void rkstep45(
	double t,
	double h,
	gsl_vector* yt,
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* yth,
	gsl_vector* err)
{
	int n = yt->size;
	gsl_vector* k0 = gsl_vector_alloc(n);	
	gsl_vector* k1 = gsl_vector_alloc(n);	
	gsl_vector* k2 = gsl_vector_alloc(n);	
	gsl_vector* k3 = gsl_vector_alloc(n);	
	gsl_vector* k4 = gsl_vector_alloc(n);	
	gsl_vector* k5 = gsl_vector_alloc(n);	
	gsl_vector* yx = gsl_vector_alloc(n);

	f(t,yt,k0);
	int i;
	double val, val_err, yti, k0i, k1i, k2i, k3i, k4i, k5i;
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		val = yti+1.0/4*k0i*h;	
		gsl_vector_set(yx,i,val);
	}

	f(t+1.0/4*h,yx,k1);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		val = yti+(3.0/32*k0i + 9.0/32*k1i)*h; 
		gsl_vector_set(yx,i,val);
	}

	f(t+3.0/8*h,yx,k2);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		k2i = gsl_vector_get(k2,i);
		val = yti+(1932.0/2197*k0i - 7200.0/2197*k1i + 7296.0/2197*k2i)*h; 
		gsl_vector_set(yx,i,val);
	}
	
	f(t+12.0/13*h,yx,k3);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		k2i = gsl_vector_get(k2,i);
		k3i = gsl_vector_get(k3,i);
		val = yti+(439.0/216*k0i - 8.0*k1i + 3680/513*k2i - 845.0/4104*k3i)*h; 
		gsl_vector_set(yx,i,val);
	}
	
	f(t+h,yx,k4);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		k2i = gsl_vector_get(k2,i);
		k3i = gsl_vector_get(k3,i);
		k4i = gsl_vector_get(k4,i);
		val = yti+(-8.0/27*k0i + 2*k1i - 3544.0/2565*k2i + 1859/4104*k3i - 11.0/40*k4i)*h; 
		gsl_vector_set(yx,i,val);
	}

	f(t+1.0/2*h,yx,k5);
	for(i = 0; i<n; i++) {
		yti = gsl_vector_get(yt,i);
		k0i = gsl_vector_get(k0,i);
		k1i = gsl_vector_get(k1,i);
		k2i = gsl_vector_get(k2,i);
		k3i = gsl_vector_get(k3,i);
		k4i = gsl_vector_get(k4,i);
		k5i = gsl_vector_get(k5,i);
		val = yti+(16.0/135*k0i + 6656.0/12825*k2i + 28561.0/56430*k3i -9.0/50*k4i + 2.0/55*k5i)*h;	
		gsl_vector_set(yth,i,val); // The best guess of the next function value 
		val_err = yti + (25.0/216*k0i + 1408.0/2565*k2i + 2197.0/4104*k3i - 1.0/5*k4i)*h;
		gsl_vector_set(err,i,val-val_err); // The error on the guess
	}

	gsl_vector_free(k0);
	gsl_vector_free(k1);
	gsl_vector_free(k2);
	gsl_vector_free(k3);
	gsl_vector_free(k4);
	gsl_vector_free(k5);
	gsl_vector_free(yx);
}

int driver(
	double b,
	double* h,
	gsl_vector* yt,
	double acc,
	double eps,
	void stepper(
		double t, double h, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err
		),
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* t_steps,
	gsl_matrix* y_steps,
	int max)
{
	int n = yt->size;
	gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);

	double power = 0.25;
	double safety = 0.95;
	double a = gsl_vector_get(t_steps,0);
	double tau_i, e_i;
	double scale;
	gsl_vector_view y_temp;

	int iter = 0;
	int i = 0;
	double t = gsl_vector_get(t_steps,i);

	while(t <= b && iter<1000) {
		iter++;
		if(t+*h>b) {
			*h = b-t;
		}
		
		y_temp = gsl_matrix_row(y_steps,i);		
		yt = &y_temp.vector;

		stepper(t,*h,yt,f,yth,err); 
		
		tau_i = (eps * norm(yth) + acc) * sqrt(*h / (b-a));
		e_i = norm(err);


		if(e_i > 0) {
			scale = pow(tau_i/e_i,power)*safety;
			*h *= scale;		
		}

		if(e_i < tau_i) {

			i++;
			t += *h;	
			gsl_vector_set(t_steps,i,t);
			gsl_matrix_set_row(y_steps,i,yth);
		}
	}

	gsl_vector_free(yth);
	gsl_vector_free(err);
	return i+1;
}

double norm(gsl_vector* x)
{
	int n = x->size;
	double length = 0;
	for(int i = 0; i<n; i++) {
		double xi = gsl_vector_get(x,i);
		length += xi*xi; 	
	}
	length = sqrt(length);
	
	return length;
}

void printer_vec(gsl_vector* vec) {
	int n = vec->size;
	for(int i = 0; i<n; i++) {
		printf("x_%d %6.3g\n",i,gsl_vector_get(vec,i));
	}

}

void printer(gsl_matrix* matter) {
	int n = matter->size1, m = matter->size2;
	for(int i = 0; i<n; i++) {
		for(int j = 0; j<m; j++) {
			printf("%8.2f ",gsl_matrix_get(matter,i,j));
		}
		printf("\n");
	}

}
