#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>
#include"qr.h"

void printer_vec(gsl_vector* vec) {
	int n = vec->size;
	for(int i = 0; i<n; i++) {
		printf("%8.4g\n",gsl_vector_get(vec,i));
	}

}

void printer(gsl_matrix* matter) {
	int n = matter->size1, m = matter->size2;
	for(int i = 0; i<n; i++) {
		for(int j = 0; j<m; j++) {
			printf("%8.4f",gsl_matrix_get(matter,i,j));
		}
		printf("\n");
	}

}

void random_vec(gsl_vector* vec) {
	int m = vec->size;
	for(int i = 0; i<m; i++) {
		double randomnr = (double) rand()/RAND_MAX;
		gsl_vector_set(vec,i,randomnr);
	}
} 

void random_mat(gsl_matrix* matter) {
	int n = matter->size1, m = matter->size2;
	for(int i = 0; i<n; i++) {
		for(int j = 0; j<m; j++) {
			double randomnr =(double) rand()/RAND_MAX;
			gsl_matrix_set(matter,i,j,randomnr);
		}
	}
}

// taken from Dmitri's slides
void backsub(const gsl_matrix* U, gsl_vector* c) {
	for(int i = c->size-1; i>=0; i--) {
		double s = gsl_vector_get(c,i);
		for(int k = i+1; k < c->size; k++) {
			s -= gsl_matrix_get(U,i,k)*gsl_vector_get(c,k);
		}
		gsl_vector_set(c,i,s/gsl_matrix_get(U,i,i));
	}	
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R) {
	for(int i = 0; i < A->size2; i++) {
		double inner_prod = 0;
		for(int j = 0; j < A->size1; j++) {
			double aji=gsl_matrix_get(A,j,i);
			inner_prod += aji*aji;
		}
		double Rii = sqrt(inner_prod);
		gsl_matrix_set(R,i,i,Rii);

		for(int j = 0; j < A->size1; j++) {
			double aji = gsl_matrix_get(A,j,i)/Rii;
			gsl_matrix_set(A,j,i,aji);
		}

		for(int j = i+1; j< A->size2; j++) {
			double qitaj = 0;
			for(int k = 0; k<A->size1; k++) {
				qitaj += gsl_matrix_get(A,k,i)*gsl_matrix_get(A,k,j);
			}			
			gsl_matrix_set(R,i,j,qitaj);

			for(int k = 0; k< A->size1; k++) {
				double akj = gsl_matrix_get(A,k,j)
					- gsl_matrix_get(A,k,i)*gsl_matrix_get(R,i,j);	
				gsl_matrix_set(A,k,j,akj);
			}
		}
	}
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x) {
	for(int i = 0; i < Q->size2; i++) {
		double sum = 0;
		for(int j = 0; j < Q->size1; j++) {
			sum += gsl_matrix_get(Q,j,i)*gsl_vector_get(b,j);
		}
		gsl_vector_set(x,i,sum);
	}

	backsub(R,x);
}

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B) {
	gsl_vector* e = gsl_vector_alloc(Q->size1);
	gsl_vector_set_zero(e);

	//copy of slides
	gsl_matrix_set_identity(B);
	for(int i = 0; i < Q->size2; i++) {
		gsl_vector_set(e,i,1);
		gsl_vector_view v = gsl_matrix_column(B,i);
		qr_gs_solve(Q,R,e,&v.vector);
		gsl_vector_set(e,i,0);
	}
	gsl_vector_free(e);
}

void covariance(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* S) {
	int n = Q->size2;
	int m = R->size1;
	assert(n==m);

	gsl_matrix* R_inv = gsl_matrix_alloc(n,n);
	gsl_matrix* I = gsl_matrix_alloc(n,n);
	gsl_matrix_set_identity(I);		
	qr_gs_inverse(I,R,R_inv);

	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,R_inv,R_inv,0.0,S);

	gsl_matrix_free(R_inv);
	gsl_matrix_free(I);
}

void error_estimate(gsl_matrix* S,gsl_vector* error){
	int n = S->size1;
	int m = S->size2;
	assert(n==m);

	for(int i = 0; i<n; i++) {
		double err_i = sqrt(gsl_matrix_get(S,i,i));
		gsl_vector_set(error,i,err_i);
	}	
}
