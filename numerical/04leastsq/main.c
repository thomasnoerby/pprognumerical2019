#include"qr.h"
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>

double fitfunctions(int i, double x){
	switch(i) {
		case 0: return log(x); break;
		case 1: return 1.0; break;
		case 2: return x; break;
		default: {
			fprintf(stderr, "fitfunctions: wrong i: %d",i);
			return NAN;
		}
	}
}

double fitfunctions1(int i, double x){
	switch(i) {
		case 0: return x; break;
		case 1: return 1; break;
		case 2: return 1.0/x; break;
		default: {
			fprintf(stderr, "fitfunctions: wrong i: %d",i);
			return NAN;
		}
	}
}

int main(int argc, char** argv) {
	assert(argc > 2);
	int n = atoi(argv[1]);
	int m = atoi(argv[2]);

	double x[n],y[n],dy[n];

	for(int i = 0; i<n; i++) {
		scanf("%lg %lg %lg",&x[i],&y[i],&dy[i]);
	}

	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_vector* b = gsl_vector_alloc(n);
	gsl_matrix* R = gsl_matrix_alloc(m,m);
	gsl_vector* res = gsl_vector_alloc(m);
	gsl_matrix* cov = gsl_matrix_alloc(m,m);
	gsl_vector* err = gsl_vector_alloc(m);

	for(int i = 0; i<n; i++){
		for(int j = 0; j<m; j++){
			double matrix_val = fitfunctions(j,x[i])/dy[i];
			gsl_matrix_set(A,i,j,matrix_val);
		}
		double vector_val = y[i]/dy[i];
		gsl_vector_set(b,i,vector_val);	
	}


	printf("A matrix:\n");
	printer(A);

	printf("Running decomp..\n");
	qr_gs_decomp(A,R);
	printf("Q after decomp:\n");
	printer(A);
	printf("R after decomp:\n");
	printer(R);
	printf("b vector:\n");
	printer_vec(b);

	printf("Running Gram-Schmidt solve\n");
	qr_gs_solve(A,R,b,res);

	printf("Resulting vector:\n");
	printer_vec(res);

	printf("Calculating covariance matrix.. \n");
	covariance(A,R,cov);
	
	printf("Covariance matrix:\n");
	printer(cov);

	printf("Calculating error estimate..\n");
	error_estimate(cov,err);
	printf("Error estimate:\n");
	printer_vec(err);
	printf("A plot can be seen in plot.svg\n");

	double coeff, err_coeff, fit, fit_lower, fit_upper;
	for(double z = 0; z<=x[n-1]; z+=0.1) {
		fit = 0;
		fit_lower = 0;
		fit_upper = 0;
		for(int i = 0; i<m; i++) {
			coeff = gsl_vector_get(res,i);
			err_coeff = gsl_vector_get(err,i);
			fit+=coeff*fitfunctions(i,z);
			fit_lower+= (coeff-err_coeff)*fitfunctions(i,z);
			fit_upper+= (coeff+err_coeff)*fitfunctions(i,z);
		}
		fprintf(stderr,"%g %g %g %g\n", z, fit, fit_lower, fit_upper);	
	}

	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_vector_free(b);
	gsl_vector_free(res);
	gsl_matrix_free(cov);
	gsl_vector_free(err);


	printf("\nCreating another fit\n");	
	n = 10;
	m = 3;	
	A = gsl_matrix_alloc(n,m);
	b = gsl_vector_alloc(n);
	R = gsl_matrix_alloc(m,m);
	res = gsl_vector_alloc(m);
	cov = gsl_matrix_alloc(m,m);
	err = gsl_vector_alloc(m);


	FILE* fdata = fopen("data1.txt","w+");
	double x1[] = {0.1, 0.145, 0.211, 0.307, 0.447, 0.649, 0.944, 1.372, 1.995, 2.9};
	double y1[] = {12.644, 9.235, 7.377, 6.46, 5.555, 5.896, 5.673, 6.964, 8.896,11.355};
	double err1[] = {0.858, 0.359,0.505, 0.403, 0.683, 0.605, 0.856,0.351, 1.083, 1.002};
	for(int i = 0; i<n; i++){
		fprintf(fdata,"%g %g %g\n",x1[i],y1[i],err1[i]);
		for(int j = 0; j<m; j++){
	 		double matrix_val = fitfunctions1(j,x1[i])/err1[i];
			gsl_matrix_set(A,i,j,matrix_val);
		}
		double vector_val = y1[i]/err1[i];
		gsl_vector_set(b,i,vector_val);	
	}
	fclose(fdata);


	printf("A matrix:\n");
	printer(A);

	printf("Running decomp..\n");
	qr_gs_decomp(A,R);
	printf("Q after decomp:\n");
	printer(A);
	printf("R after decomp:\n");
	printer(R);
	printf("b vector:\n");
	printer_vec(b);

	printf("Running Gram-Schmidt solve\n");
	qr_gs_solve(A,R,b,res);

	printf("Resulting vector:\n");
	printer_vec(res);

	printf("Calculating covariance matrix.. \n");
	covariance(A,R,cov);
	
	printf("Covariance matrix:\n");
	printer(cov);

	printf("Calculating error estimate..\n");
	error_estimate(cov,err);
	printf("Error estimate:\n");
	printer_vec(err);
	printf("A plot can be seen in plot1.svg\n");

	FILE* fp = fopen("fit1.txt","w+");
	double coeff1, err_coeff1, fit1, fit_lower1, fit_upper1;
	for(double z = 0; z<=x1[n-1]; z+=0.1) {
		fit1 = 0;
		fit_lower1 = 0;
		fit_upper1 = 0;
		for(int i = 0; i<m; i++) {
			coeff1 = gsl_vector_get(res,i);
			err_coeff1 = gsl_vector_get(err,i);
			fit1+=coeff1*fitfunctions1(i,z);
			fit_lower1+= (coeff1-err_coeff1)*fitfunctions1(i,z);
			fit_upper1+= (coeff1+err_coeff1)*fitfunctions1(i,z);
		}
		fprintf(fp,"%g %g %g %g\n", z, fit1, fit_lower1, fit_upper1);	
	}
	fclose(fp);

	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_vector_free(b);
	gsl_vector_free(res);
	gsl_matrix_free(cov);
	gsl_vector_free(err);

	return 0;
}
