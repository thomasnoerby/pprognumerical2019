#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
void backsub(const gsl_matrix* U, gsl_vector* c);
void backsub_matrix(const gsl_matrix* U, gsl_matrix* M);
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void random_mat(gsl_matrix* matter);
void random_vec(gsl_vector* vec);
void printer(gsl_matrix* matter);
void printer_vec(gsl_vector* vec);
void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x);
void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B);
void covariance(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* S);
void error_estimate(gsl_matrix* S,gsl_vector* error);
