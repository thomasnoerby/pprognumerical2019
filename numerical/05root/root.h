#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
void newton_with_jacobian(
	void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J),
	gsl_vector* x,
	double epsilon
);
void newton(
	void f(const gsl_vector* x, gsl_vector* fx),
	gsl_vector* x,
	double dx,
	double epsilon
);
void jacobian(gsl_vector* fx, gsl_vector* x, double dx, gsl_matrix* J,
	void f(const gsl_vector* x, gsl_vector* fx),
	int* noOfCalls);
void backsub(const gsl_matrix* U, gsl_vector* c);
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x);
void printer_vec(gsl_vector* vec);
void printer(gsl_matrix* matter);
void switch_sign(gsl_vector* fx);
double norm(gsl_vector* fx);
