#include"root.h"
#include<math.h>
#include<limits.h>
#include<float.h>

void sys1(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
void sys1_no_jac(const gsl_vector* x, gsl_vector* fx);
void sys2(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
void sys2_no_jac(const gsl_vector* x, gsl_vector* fx);
void sys3(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J);
void sys3_no_jac(const gsl_vector* x, gsl_vector* fx);
int root(double x_start, double y_start, 
	int func(const gsl_vector* z, void* params, gsl_vector* f));
int root_equation_1(const gsl_vector* z, void* params, gsl_vector* f);
int root_equation_2(const gsl_vector* z, void* params, gsl_vector* f);
int root_equation_3(const gsl_vector* z, void* params, gsl_vector* f);

int main() {
// Ændrer vektorprint til linjeprint
// Giv funktionsværdien i det punktet
	int n = 2;
	int x_start = 4;
	int y_start = 5;

	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector_set(x,0,x_start);
	gsl_vector_set(x,1,y_start);

	double epsilon = 1e-6;

	printf("Root-finding with analytical Jacobian\n");
	printf("First system of equations(A2):\n");	
	printf("Start:\n");
	printer_vec(x);

	newton_with_jacobian(
		sys1,
		x,
		epsilon
	);

	printf("Converged to extremum at:\n");
	printer_vec(x);

	printf("Second system: the Rosenbrock function\n");	
	gsl_vector_set(x,0,-3);
	gsl_vector_set(x,1,-10);

	printf("Start:\n");
	printer_vec(x);

	newton_with_jacobian(
		sys2,
		x,
		epsilon
	);

	printf("Converged to extremum at:\n");
	printer_vec(x);

	printf("Third system: the Himmelblau function\n");

	gsl_vector_set(x,0,10);
	gsl_vector_set(x,1,20);

	printf("Start:\n");
	printer_vec(x);

	newton_with_jacobian(
		sys3,
		x,
		epsilon
	);

	printf("Converged to extremum at:\n");
	printer_vec(x);

	printf("\nRoot-finding with numerical Jacobian\n");
	
	double dx = sqrt(DBL_EPSILON);
	gsl_vector_set(x,0,x_start); 
	gsl_vector_set(x,1,y_start); 

	printf("First system of equations(A2):\n");	
	printf("Start:\n");
	printer_vec(x);

	newton(
		sys1_no_jac,
		x,
		dx,
		epsilon
	);

	printf("Converged to extremum at:\n");
	printer_vec(x);


	printf("Second system: the Rosenbrock valley\n");

	gsl_vector_set(x,0,-3);
	gsl_vector_set(x,1,-10);

	printf("Start:\n");
	printer_vec(x);

	newton(
		sys2_no_jac,
		x,
		dx,
		epsilon
	);

	printf("Converged to extremum at:\n");
	printer_vec(x);

	printf("Third system: the Himmelblau function\n");

	gsl_vector_set(x,0,10);
	gsl_vector_set(x,1,20);

	printf("Start:\n");
	printer_vec(x);

	newton(
		sys3_no_jac,
		x,
		dx,
		epsilon
	);

	printf("Converged to extremum at:\n");
	printer_vec(x);

	gsl_vector_free(x);

	printf("\nRoot-finding with gsl routines without analytical Jacobian\n");
	printf("First system of equations(A2):\n");	
	root(x_start,y_start,root_equation_1);

	printf("\nSecond system: the Rosenbrock valley\n");
	root(x_start,y_start,root_equation_2);

	printf("\nThird system: the Himmelblau function\n");
	root(x_start,y_start,root_equation_3);
	
	return 0;
}

void sys1(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) {
	int A = 10000;
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);	
	double f0 = A*x_val*y_val-1;
	double f1 = exp(-x_val)+exp(-y_val)-1-1.0/A;

	gsl_vector_set(fx,0,f0);
	gsl_vector_set(fx,1,f1);
	
	double df0_dx = A*y_val;
	double df0_dy = A*x_val;
	double df1_dx = -exp(-x_val);
	double df1_dy = -exp(-y_val);

	gsl_matrix_set(J,0,0,df0_dx);
	gsl_matrix_set(J,0,1,df0_dy);
	gsl_matrix_set(J,1,0,df1_dx);
	gsl_matrix_set(J,1,1,df1_dy);
}

void sys1_no_jac(const gsl_vector* x, gsl_vector* fx) {
	int A = 10000;
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);	
	double f0 = A*x_val*y_val-1;
	double f1 = exp(-x_val)+exp(-y_val)-1-1.0/A;

	gsl_vector_set(fx,0,f0);
	gsl_vector_set(fx,1,f1);
}

void sys2(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) {
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);	
	double f0 = 2*x_val-2-200*(y_val-x_val*x_val)*2*x_val; 
	double f1 = 200*(y_val-x_val*x_val); 

	gsl_vector_set(fx,0,f0);
	gsl_vector_set(fx,1,f1);
	
	double df0_dx = 2-400*y_val+1200*x_val*x_val;
	double df0_dy = -400*x_val;
	double df1_dx = -400*x_val;
	double df1_dy = 200; 

	gsl_matrix_set(J,0,0,df0_dx);
	gsl_matrix_set(J,0,1,df0_dy);
	gsl_matrix_set(J,1,0,df1_dx);
	gsl_matrix_set(J,1,1,df1_dy);
}

void sys2_no_jac(const gsl_vector* x, gsl_vector* fx) {
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);	
	double f0 = 2*x_val-2-200*(y_val-x_val*x_val)*2*x_val; 
	double f1 = 200*(y_val-x_val*x_val); 

	gsl_vector_set(fx,0,f0);
	gsl_vector_set(fx,1,f1);
}

void sys3(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) {
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);	
	double f0 = 4*x_val*(x_val*x_val+y_val-11)+2*(x_val+y_val*y_val-7); 
	double f1 = 2*(x_val*x_val+y_val-11)+4*y_val*(x_val+y_val*y_val-7);

	gsl_vector_set(fx,0,f0);
	gsl_vector_set(fx,1,f1);
	
	double df0_dx = 4*y_val-44+12*x_val*x_val+2;
	double df0_dy = 4*x_val+4*y_val;
	double df1_dx = 4*x_val+4*y_val;
	double df1_dy = 2+4*x_val-4*7+12*y_val*y_val; 

	gsl_matrix_set(J,0,0,df0_dx);
	gsl_matrix_set(J,0,1,df0_dy);
	gsl_matrix_set(J,1,0,df1_dx);
	gsl_matrix_set(J,1,1,df1_dy);
}

void sys3_no_jac(const gsl_vector* x, gsl_vector* fx) {
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);	
	double f0 = 4*x_val*(x_val*x_val+y_val-11)+2*(x_val+y_val*y_val-7); 
	double f1 = 2*(x_val*x_val+y_val-11)+4*y_val*(x_val+y_val*y_val-7);

	gsl_vector_set(fx,0,f0);
	gsl_vector_set(fx,1,f1);
}

int root_equation_1 (const gsl_vector* z, void* params, gsl_vector* f)
{
	int A = 10000;
	double x_val = gsl_vector_get(z,0);
	double y_val = gsl_vector_get(z,1);	
	double f0 = A*x_val*y_val-1;
	double f1 = exp(-x_val)+exp(-y_val)-1-1.0/A;

	gsl_vector_set(f,0,f0);
	gsl_vector_set(f,1,f1);

	return GSL_SUCCESS;
}

int root_equation_2 (const gsl_vector* z, void* params, gsl_vector* f)
{
	double x0 = gsl_vector_get(z,0);
	double y0 = gsl_vector_get(z,1);
	double fx = 2*(-1+x0-200*(y0-x0*x0)*x0);
	double fy = 200*(y0-x0*x0);
	
	gsl_vector_set(f,0,fx);
	gsl_vector_set(f,1,fy);

	return GSL_SUCCESS;
}

int root_equation_3 (const gsl_vector* z, void* params, gsl_vector* f)
{
	double x_val = gsl_vector_get(z,0);
	double y_val = gsl_vector_get(z,1);	
	double f0 = 4*x_val*(x_val*x_val+y_val-11)+2*(x_val+y_val*y_val-7); 
	double f1 = 2*(x_val*x_val+y_val-11)+4*y_val*(x_val+y_val*y_val-7);

	gsl_vector_set(f,0,f0);
	gsl_vector_set(f,1,f1);

	return GSL_SUCCESS;
}
