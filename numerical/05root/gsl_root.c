#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#define TYPE gsl_multiroot_fsolver_hybrids

int root(double x_start, double y_start, 
	int func(const gsl_vector* z, void* params, gsl_vector* f))
{
	gsl_multiroot_function F;
	F.f = func;
	F.n = 2;
	F.params = NULL;

	gsl_vector* x = gsl_vector_alloc(F.n);
	gsl_vector_set(x,0,x_start);
	gsl_vector_set(x,1,y_start);

	gsl_multiroot_fsolver* S;
	S = gsl_multiroot_fsolver_alloc(TYPE,F.n);
	gsl_multiroot_fsolver_set(S,&F,x);

	int flag, iter = 0;
	do {
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual((*S).f, 1e-12);
		iter++;
	}while(flag == GSL_CONTINUE);

	double result_x = gsl_vector_get((*S).x,0);
	double result_y = gsl_vector_get((*S).x,1);

	printf("Converged to extremum point at f(%g,%g)\n"
		,result_x,result_y);
	printf("The algorithm converged after %i iterations\n",iter);

	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(x);
	return GSL_SUCCESS;
}
