#include"root.h"
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>
void newton_with_jacobian(
	void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J),
	gsl_vector* x,
	double epsilon)
{
	int n = x->size;
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* delta_x = gsl_vector_alloc(n);
	gsl_vector* intermediate = gsl_vector_alloc(n);

	int noOfCalls = 0;

	int iter = 0;
	double length;
	double lambda;
	do {
		f(x,fx,J);

		noOfCalls++;

		length = norm(fx);
		iter++;
		switch_sign(fx);
		
		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,fx,delta_x);

		lambda = 2;

		do {
			lambda /= 2;
			gsl_blas_dcopy(x,intermediate);
			gsl_blas_daxpy(lambda,delta_x,intermediate);
			f(intermediate,fx,J);
			noOfCalls++;
		}
		while(norm(fx) > (1-lambda/2)*length && lambda > 1.0/64);
		gsl_blas_daxpy(lambda,delta_x,x);
		length = norm(fx);
	}
	while(length>epsilon);
	printf("Converged after %d iterations and %d function calls\n",iter, noOfCalls);

	gsl_vector_free(fx);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(delta_x);
	gsl_vector_free(intermediate);
}

void newton(
	void f(const gsl_vector* x, gsl_vector* fx),
	gsl_vector* x,
	double dx,
	double epsilon)
{
	int n = x->size;
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* delta_x = gsl_vector_alloc(n);
	gsl_vector* intermediate = gsl_vector_alloc(n);

	double lambda;
	double length;

	int iter = 0;
	int noOfCalls = 0;
	do {
		f(x,fx);
		noOfCalls++;
		length = norm(fx);

		iter++;
		jacobian(fx,x,dx,J,f, &noOfCalls);
		switch_sign(fx);
		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,fx,delta_x);

		lambda = 2;
		do {
			lambda /= 2;
			gsl_vector_memcpy(intermediate,x);
			gsl_blas_daxpy(lambda,delta_x,intermediate);
			f(intermediate,fx);
			noOfCalls++;
		}
		while(norm(fx) > (1-lambda/2)*length && lambda > dx);
		gsl_blas_daxpy(lambda,delta_x,x);
		length = norm(fx);
	}
	while(length>epsilon);
	printf("Converged after %d iterations and %d function calls\n",iter, noOfCalls);

	gsl_vector_free(fx);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(delta_x);
	gsl_vector_free(intermediate);
}

void jacobian(gsl_vector* fx, gsl_vector* x, double dx, gsl_matrix* J,
	void f(const gsl_vector* x, gsl_vector* fx),
	int* noOfCalls) 
{
	int n = fx->size;
	gsl_vector* x_cpy = gsl_vector_alloc(n);
	gsl_vector* fx_cpy = gsl_vector_alloc(n);
	gsl_vector_memcpy(x_cpy,x);
	gsl_vector_memcpy(fx_cpy,fx);

	for(int i = 0; i<n; i++) {
		double f_i = gsl_vector_get(fx,i);
		for(int j = 0; j<n; j++) {
			double x_j = gsl_vector_get(x,j);
			gsl_vector_set(x_cpy,j,x_j+dx);

			f(x_cpy,fx_cpy);
			noOfCalls++;
			double f_i_inc = gsl_vector_get(fx_cpy,i);
			double Jij = (f_i_inc-f_i)/dx;
			gsl_matrix_set(J,i,j,Jij);	

			gsl_vector_set(x_cpy,j,x_j);
		}
	}

	gsl_vector_free(fx_cpy);
	gsl_vector_free(x_cpy);
}

void switch_sign(gsl_vector* fx)
{
	int n = fx->size;
	for(int i = 0; i<n; i++) {
		double fxi = gsl_vector_get(fx,i);
		gsl_vector_set(fx,i,-fxi);
	}
}

double norm(gsl_vector* fx)
{
	int n = fx->size;
	double length = 0;
	for(int i = 0; i<n; i++) {
		double fxi = gsl_vector_get(fx,i);
		length += fxi*fxi; 	
	}
	length = sqrt(length);
	
	return length;
}

// taken from Dmitri's slides
void backsub(const gsl_matrix* U, gsl_vector* c) {
	for(int i = c->size-1; i>=0; i--) {
		double s = gsl_vector_get(c,i);
		for(int k = i+1; k < c->size; k++) {
			s -= gsl_matrix_get(U,i,k)*gsl_vector_get(c,k);
		}

		gsl_vector_set(c,i,s/gsl_matrix_get(U,i,i));
	}	
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R) {
	for(int i = 0; i < A->size2; i++) {
		double inner_prod = 0;
		for(int j = 0; j < A->size1; j++) {
			double aji=gsl_matrix_get(A,j,i);
			inner_prod += aji*aji;
		}
		double Rii = sqrt(inner_prod);
		gsl_matrix_set(R,i,i,Rii);

		for(int j = 0; j < A->size1; j++) {
			double aji = gsl_matrix_get(A,j,i)/Rii;
			gsl_matrix_set(A,j,i,aji);
		}

		for(int j = i+1; j< A->size2; j++) {
			double qitaj = 0;
			for(int k = 0; k<A->size1; k++) {
				qitaj += gsl_matrix_get(A,k,i)*gsl_matrix_get(A,k,j);
			}			
			gsl_matrix_set(R,i,j,qitaj);

			for(int k = 0; k< A->size1; k++) {
				double akj = gsl_matrix_get(A,k,j)
					- gsl_matrix_get(A,k,i)*gsl_matrix_get(R,i,j);	
				gsl_matrix_set(A,k,j,akj);
			}
		}
	}
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x) {
	for(int i = 0; i < Q->size2; i++) {
		double sum = 0;
		for(int j = 0; j < Q->size1; j++) {
			sum += gsl_matrix_get(Q,j,i)*gsl_vector_get(b,j);
		}
		gsl_vector_set(x,i,sum);
	}

	backsub(R,x);
}

void printer_vec(gsl_vector* vec) {
	int n = vec->size;
	for(int i = 0; i<n; i++) {
		printf("x_%d %6.3g\n",i,gsl_vector_get(vec,i));
	}

}

void printer(gsl_matrix* matter) {
	int n = matter->size1, m = matter->size2;
	for(int i = 0; i<n; i++) {
		for(int j = 0; j<m; j++) {
			printf("%8.2f ",gsl_matrix_get(matter,i,j));
		}
		printf("\n");
	}

}
