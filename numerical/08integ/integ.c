#include"integ.h"
#include<gsl/gsl_vector.h>
#include<math.h>
#include<assert.h>
#include<stdio.h>

double integrate(double f(double), double a, double b, double acc, double eps, double *err)
{
	int iter = 0;
	int depth = 0;
	*err = 0;

	double scale = b-a;
	double f2 = f(a+scale*2.0/6);
	double f3 = f(a+scale*4.0/6);

	double Q = integrator(f,a,b,acc,eps,err,f2,f3,&iter,depth);
	printf("Completed integral after %d evaluations\n",iter);

	return Q;
}

double integrator(double f(double), double a, double b, double acc, double eps, double *err, double f2, double f3, int* iter,int depth)
{
	assert(b > a);
	assert(depth < 1000000);

	*iter += 1;
	double Q = 0;
	double q = 0;

	double scale = b-a;
	double f1 = f(a+scale*1.0/6);
	double f4 = f(a+scale*5.0/6);

	Q = (f1*2 + f2 + f3 + f4*2)/6*scale;
	q = (f1 + f2 + f3 + f4)/4*scale;

	*err = fabs(Q-q);
	double tolerance = acc + eps*fabs(Q);

	if(*err > tolerance) {
		double err1, err2;
		double mid = (a+b)/2;
		double rescaled_acc = acc/sqrt(2);
		double first_half = integrator(f,a,mid,rescaled_acc,eps,&err1,f1,f2,iter,depth+1);
		double second_half = integrator(f,mid,b,rescaled_acc,eps,&err2,f3,f4,iter,depth+1);
		*err = sqrt(err1*err1+err2*err2); // lend from Signe Kyrkjebø
		Q = first_half+second_half;
	}

	return Q;
}

// Lend from Signe Kyrkjebø
double integrate_clen_curt(double f(double), double a, double b, double acc, double eps, double *err)
{
	double scaled_func(double t){
		double input = (a+b)/2 + (a-b)/2 * cos(t);
		double scale = sin(t) * (b-a)/2;
		return f(input)*scale;
	}

	double new_a = 0;
	double new_b = M_PI;
	return integrate(scaled_func,new_a,new_b,acc,eps,err);
}

double integrate_infinite(double f(double), double a, double b, double acc, double eps, double *err)
{
	assert(a != b);
	int a_check = (a == -INFINITY);
	int b_check = (b == INFINITY);

	if(!a_check && !b_check) { // no infinities
		return integrate(f,a,b,acc,eps,err);
	}

	double new_a, new_b, input, scale;
	if(a_check) {
		if(b_check) { // both are infinite
			double scaled_func(double t) {
				input = t/(1-t*t);
				scale = (1+t*t)/(1-t*t)/(1-t*t);
				return f(input)*scale;
			}
			
			new_a = -1, new_b = 1;
			return integrate(scaled_func,new_a,new_b,acc,eps,err);
		}
		else { // only a is inifinite
			double scaled_func(double t) {
				input = b+t/(1+t);
				scale = 1/(1+t)/(1+t);
				return f(input)*scale;
			}	

			new_a = -1, new_b = 0;
			return integrate(scaled_func,new_a,new_b,acc,eps,err);
		}
	}
	else { // only b is infinite
		double scaled_func(double t) {
			input = a+t/(1-t);
			scale = 1/(1-t)/(1-t);
			return f(input)*scale;
		}

		new_a = 0, new_b = 1;
		return integrate(scaled_func,new_a,new_b,acc,eps,err);
	}

}


void set_init(gsl_vector* x, gsl_vector* w, gsl_vector* v, double a, double b)
{
	double scale = b-a;
	gsl_vector_set(x,0,a+scale*1.0/6);
	gsl_vector_set(x,1,a+scale*2.0/6);
	gsl_vector_set(x,2,a+scale*4.0/6);
	gsl_vector_set(x,3,a+scale*5.0/6);

	gsl_vector_set(w,0,scale*2.0/6);
	gsl_vector_set(w,1,scale*1.0/6);
	gsl_vector_set(w,2,scale*1.0/6);
	gsl_vector_set(w,3,scale*2.0/6);

	gsl_vector_set_all(v,scale*1.0/4);
}
