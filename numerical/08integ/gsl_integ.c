#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<math.h>

double gsl_qags(double f(double t, void* params), double a, double b, double abs, double eps, double *error)
{
	int limit = 20000;
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);

	gsl_function F;
	F.function = f;
	F.params = NULL;

	double result;
	int flag = gsl_integration_qags(&F, a, b, abs, eps, limit, w, &result, error);

	gsl_integration_workspace_free(w);

	if(flag != GSL_SUCCESS) return NAN;
	return result;
}

double gsl_qagi(double f(double t, void* params), double abs, double eps, double *error)
{
	int limit = 20000;
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);

	gsl_function F;
	F.function = f;
	F.params = NULL;

	double result;
	int flag = gsl_integration_qagi(&F, abs, eps, limit, w, &result, error);

	gsl_integration_workspace_free(w);

	if(flag != GSL_SUCCESS) return NAN;
	return result;
}

double gsl_qagiu(double f(double t, void* params), double a, double abs, double eps, double *error)
{
	int limit = 20000;
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);

	gsl_function F;
	F.function = f;
	F.params = NULL;

	double result;
	int flag = gsl_integration_qagiu(&F, a, abs, eps, limit, w, &result, error);

	gsl_integration_workspace_free(w);

	if(flag != GSL_SUCCESS) return NAN;
	return result;
}

double gsl_qagil(double f(double t, void* params), double b, double abs, double eps, double *error)
{
	int limit = 20000;
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);

	gsl_function F;
	F.function = f;
	F.params = NULL;

	double result;
	int flag = gsl_integration_qagil(&F, b, abs, eps, limit, w, &result, error);

	gsl_integration_workspace_free(w);

	if(flag != GSL_SUCCESS) return NAN;
	return result;
}
