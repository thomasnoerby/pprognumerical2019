#include<stdio.h>
#include"integ.h"
#include"gsl_integ.h"
#include<math.h>

int main() {
	printf("Exercise A\n");
	int iter = 0;
	double func1(double x)
	{
		iter++;
		return sqrt(x);
	}

	double func2(double x)
	{
		iter++;
		return 1.0/sqrt(x);
	}

	double func3(double x)
	{
		iter++;
		return log(x)/sqrt(x);
	}

	double func4(double x)
	{
		iter++;
		return 4*sqrt(1-(1-x)*(1-x));
	}

	double a=0, b=1, err, acc=1e-5, eps=1e-5;

	printf("Calculating sqrt(x) from x = 0 to 1\n");
	double Q1 = integrate(func1, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be 2/3\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q1,err);
	printf("\n");

	iter = 0;
	printf("Calculating 1.0/sqrt(x) from x = 0 to 1\n");
	double Q2 = integrate(func2, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be 2\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q2,err);
	printf("\n");

	iter = 0;
	printf("Calculating log(x)/sqrt(x) from x = 0 to 1\n");
	double Q3 = integrate(func3, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be -4\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q3,err);
	printf("\n");

	iter = 0;
	printf("Calculating 4*sqrt(1-(1-x)*(1-x)) from x = 0 to 1\n");
	double Q4 = integrate(func4, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be PI\n");
	printf("The result of the integral is %9.8g with an estimated error of %g\n",Q4,err);
	printf("\n");


	printf("\nExercise B using Clenshaw-Curtis quadrature\n");
	iter = 0;
	printf("Calculating sqrt(x) from x = 0 to 1\n");
	double Q5 = integrate_clen_curt(func1, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be 2/3\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q5,err);
	printf("\n");

	iter = 0;
	printf("Calculating 1.0/sqrt(x) from x = 0 to 1\n");
	double Q6 = integrate_clen_curt(func2, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be 2\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q6,err);
	printf("\n");

	iter = 0;
	printf("Calculating log(x)/sqrt(x) from x = 0 to 1\n");
	double Q7 = integrate_clen_curt(func3, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be -4\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q7,err);
	printf("\n");

	iter = 0;
	printf("Calculating 4*sqrt(1-(1-x)*(1-x)) from x = 0 to 1\n");
	double Q8 = integrate_clen_curt(func4, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be PI\n");
	printf("The result of the integral is %9.8g with an estimated error of %g\n",Q8,err);

	
	printf("\nComparing with GSL-qags routine\n");
	double func1_gsl(double x, void* params)
	{
		iter++;
		return sqrt(x);
	}

	double func2_gsl(double x, void* params)
	{
		iter++;
		return 1.0/sqrt(x);
	}

	double func3_gsl(double x, void* params)
	{
		iter++;
		return log(x)/sqrt(x);
	}

	double func4_gsl(double x, void* params)
	{
		iter++;
		return 4*sqrt(1-(1-x)*(1-x));
	}
	iter = 0;
	printf("Calculating sqrt(x) from x = 0 to 1\n");
	double Q9 = gsl_qags(func1_gsl, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be 2/3\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q9,err);
	printf("\n");

	iter = 0;
	printf("Calculating 1.0/sqrt(x) from x = 0 to 1\n");
	double Q10 = gsl_qags(func2_gsl, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be 2\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q10,err);
	printf("\n");

	iter = 0;
	printf("Calculating log(x)/sqrt(x) from x = 0 to 1\n");
	double Q11 = gsl_qags(func3_gsl, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be -4\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q11,err);
	printf("\n");

	iter = 0;
	printf("Calculating 4*sqrt(1-(1-x)*(1-x)) from x = 0 to 1\n");
	double Q12 = gsl_qags(func4_gsl, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be PI\n");
	printf("The result of the integral is %9.8g with an estimated error of %g\n",Q12,err);
	printf("\n");


	printf("\nExercise C: infinite integrals\n");
	double inf_func1(double x)
	{
		iter++;
		return exp(-x*x);
	}

	double inf_func2(double x)
	{
		iter++;
		return sin(x)*sin(x)/x/x;
	}

	double inf_func3(double x)
	{
		iter++;
		return x*exp(-x*x);
	}

	iter = 0;
	a = -INFINITY, b = INFINITY;
	printf("Calculating exp(-x*x) from x = -inf to inf\n");
	double Q13 = integrate_infinite(inf_func1, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be sqrt(PI) = %g\n",sqrt(M_PI));
	printf("The result of the integral is %9.8g with an estimated error of %g\n",Q13,err);
	printf("\n");

	iter = 0;
	a = 0, b = INFINITY;
	printf("Calculating sin(x)*sin(x)/(x*x) from x = 0 to inf\n");
	double Q14 = integrate_infinite(inf_func2, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be PI/2 = %g\n",M_PI/2);
	printf("The result of the integral is %9.8g with an estimated error of %g\n",Q14,err);
	printf("\n");

	iter = 0;
	a = -INFINITY, b = 0;
	printf("Calculating x*exp(-x*x) from x = -inf to 0\n");
	double Q15 = integrate_infinite(inf_func3, a, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be -1/2\n");
	printf("The result of the integral is %9.8g with an estimated error of %g\n",Q15,err);

	
	printf("\nComparing with GSL-qagi routine\n");
	double gsl_inf_func1(double x, void* params)
	{
		iter++;
		return exp(-x*x);
	}

	double gsl_inf_func2(double x, void* params)
	{
		iter++;
		return sin(x)*sin(x)/x/x;
	}

	double gsl_inf_func3(double x, void* params)
	{
		iter++;
		return x*exp(-x*x);
	}

	printf("Calculating exp(-x*x) from x = -inf to inf\n");
	double Q16 = gsl_qagi(gsl_inf_func1, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be sqrt(PI) = %g\n",sqrt(M_PI));
	printf("The result of the integral is %g with an estimated error of %g\n",Q16,err);
	printf("\n");

	b = 0;
	printf("Calculating sin(x)*sin(x)/(x*x) from x = -inf to 0\n");
	double Q17 = gsl_qagil(gsl_inf_func2, b, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be PI/2 = %g\n",M_PI/2);
	printf("The result of the integral is %g with an estimated error of %g\n",Q17,err);
	printf("\n");

	a = 0;
	printf("Calculating x*exp(-x*x) from x = 0 to inf\n");
	double Q18 = gsl_qagiu(gsl_inf_func3, a, acc, eps, &err);
	printf("The integral completed after %d function calls\n",iter);
	printf("The integral should be -1/2\n");
	printf("The result of the integral is %g with an estimated error of %g\n",Q18,err);
	printf("\n");


	return 0;
}

