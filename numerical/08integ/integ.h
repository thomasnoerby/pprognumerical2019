#include<gsl/gsl_vector.h>

double integrate(double f(double), double a, double b, double acc, double eps, double *err);
double integrator(double f(double), double a, double b, double acc, double eps, double *err, double f2, double f3, int* iter, int depth);
double integrate_clen_curt(double f(double), double a, double b, double acc, double eps, double *err);
double integrate_infinite(double f(double), double a, double b, double acc, double eps, double *err);
void set_init(gsl_vector* x, gsl_vector* w, gsl_vector* v, double a, double b);
