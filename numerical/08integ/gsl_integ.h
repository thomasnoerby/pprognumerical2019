double gsl_qags(double f(double t, void*), double a, double b, double abs, double eps, double *error);
double gsl_qagi(double f(double t, void* params), double abs, double eps, double *error);
double gsl_qagiu(double f(double t, void* params), double a, double abs, double eps, double *error);
double gsl_qagil(double f(double t, void* params), double b, double abs, double eps, double *error);
