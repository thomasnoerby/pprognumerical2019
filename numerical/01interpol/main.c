#include<stdio.h>
#include<math.h>
#include"linterp.h"
#include"quadinterp.h"
#include"cubicinterp.h"
#include<gsl/gsl_spline.h>

int main() {
	FILE* fout = fopen("out.txt","w+");
	fprintf(fout,"Tabulating functions: y = x*x, g = 3*(1+cos(x)), h = 1/3*x*x*x, b = cos(x)\n");
	int n = 10;
	double x[n], y[n], g[n], h[n], b[n];
	for(int i=0; i<n; i++) {
		x[i] = i;
		y[i] = x[i]*x[i];
		g[i] = 3*(1+cos(x[i]));
		h[i] = 1.0/3*x[i]*x[i]*x[i];
		b[i] = cos(x[i]);
		fprintf(stderr,"%g %g %g %g %g\n",x[i],y[i],g[i],h[i],b[i]);
	}
	
	fprintf(fout,"Calculating linear splines for some of the tabulated functions\n");
	for(double z = 0; z<=x[n-1]; z+=0.1) {	
		double spline_y = linterp(n,x,y,z);
		double spline_g = linterp(n,x,g,z);
		double integ = linterp_integ(n,x,y,z);
		printf("%g %g %g %g\n", z, spline_y, spline_g, integ);
	}

	fprintf(fout,"Calculating quadratic splines for some of the tabulated functions\n");
	FILE *fp = fopen("qspline.data","w+");
	qspline* qspline = qspline_alloc(n,x,b);
	for(double z = 0; z<x[n-1]; z+= 0.1) {
		double eval = qspline_evaluate(qspline,z);
		double der = qspline_derivative(qspline,z);
		double integral = qspline_integral(qspline,z);
		fprintf(fp,"%g %g %g %g\n",z,eval,der,integral);		
	}

	fprintf(fout,"Calculating cubic splines for some of the tabulated functions\n");
	FILE *fp2 = fopen("cubspline.data","w+");
	cubspline* cubspline = cubspline_alloc(n,x,b);
	for(double z = 0; z<x[n-1]; z+= 0.1) {
		double eval = cubspline_evaluate(cubspline,z);
		double der = cubspline_derivative(cubspline,z);
		double integral = cubspline_integral(cubspline,z);
		fprintf(fp2,"%g %g %g %g\n",z,eval,der,integral);		
	}

	fprintf(fout,"Calculating gsl cubic spline for some of the tabulated functions\n");
	FILE *fp3 = fopen("gsl.data","w+");
	gsl_interp_accel *acc = gsl_interp_accel_alloc();
	gsl_spline* gsl_int = gsl_spline_alloc(gsl_interp_cspline,n);
	gsl_spline_init(gsl_int,x,b,n);
	for(double z = 0; z<x[n-1]; z+= 0.1) {
		double eval = gsl_spline_eval(gsl_int,z,acc);
		fprintf(fp3,"%g %g\n",z,eval);
	}

	fprintf(fout,"Linear splines of functions can be seen in plot.svg\n");
	fprintf(fout,"Quadratic splines can be seen in quadplot.svg \n");
	fprintf(fout,"Cubic splines can be seen in cubic.svg\n");
	fprintf(fout,"A comparison between the cubic spline and a gsl cubic spline can be seen in compare.svg\n");

	
	gsl_spline_free(gsl_int);
	gsl_interp_accel_free(acc);
	fclose(fp);
	fclose(fp2);
	fclose(fp3);
	fclose(fout);
	qspline_free(qspline);
	cubspline_free(cubspline);
	
	return 0;
}
