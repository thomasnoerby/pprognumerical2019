#include<stdlib.h>
#include<stdio.h>
#include<assert.h>
typedef struct {int n; double *x, *y, *b, *c;} qspline;

qspline* qspline_alloc(int n, double* x, double* y){
	qspline* qs = (qspline*) malloc(sizeof(qspline));
	double* x_malloc = (double*) malloc(n*sizeof(double));
	double* y_malloc = (double*) malloc(n*sizeof(double));
	double* b = (double*) malloc((n-1)*sizeof(double));
	double* c = (double*) malloc((n-1)*sizeof(double));

	qs->x = x_malloc;
	qs->y = y_malloc;

	for(int i = 0; i<n; i++) {
		qs->x[i] = x[i];
		qs->y[i] = y[i];
	}

	double p[n-1], h[n-1];
	for(int i = 0; i<n-1; i++) {
		h[i] = x[i+1]-x[i];
		p[i] = (y[i+1]-y[i])/h[i];
	
	}

	// forward pass
	c[0] = 0;
	for(int i = 0; i<n-2; i++) {
		c[i+1] = 1.0/h[i+1]*(p[i+1]-p[i]-c[i]*h[i]);
	}

	// backward pass
	c[n-2] /= 2;
	for(int i = n-3; i>=0; i--) {
		c[i] = 1.0/h[i]*(p[i+1]-p[i]-c[i+1]*h[i+1]);
	}

	for(int i = 0; i<n-1; i++) {
		b[i] = p[i]-c[i]*h[i];
	}

	qs->n = n;
	qs->b = b;
	qs->c = c;

	return qs;	
}

double qspline_evaluate(qspline* qs, double z){
	double* x = qs->x;
	double* y = qs->y;
	double* b = qs->b;
	double* c = qs->c;
	int n = qs->n; 
	assert(z>=x[0] && z<= x[n-1]);

	int i = 0, j = n-1; 
	while(j-i>1) {
		int mid = (i+j)/2;
		if(z>x[mid]) {i = mid;}
		else {j = mid;}
	}

	double spline = y[i]+b[i]*(z-x[i])+c[i]*(z-x[i])*(z-x[i]);

	return spline;
}

double qspline_derivative(qspline *qs, double z) {
	double* x = qs->x;
	double* b = qs->b;
	double* c = qs->c;
	int n = qs->n; 

	int i = 0, j = n-1; 
	while(j-i>1) {
		int mid = (j+i)/2;
		if(z>x[mid]) {i = mid;}
		else {j = mid;}
	}

	double qspline_der = b[i]+c[i]*2*(z-x[i]);  

	return qspline_der;
}

double qspline_integral(qspline *qs, double z) {
	double* x = qs->x;
	double* y = qs->y;
	double* b = qs->b;
	double* c = qs->c;
	double n = qs->n; 

	int k = 0, j = n-1; 
	while(j-k>1) {
		int mid = (k+j)/2;
		if(z>x[mid]) {k = mid;}
		else {j = mid;}
	}

	double qspline_int = 0;
	for(int i = 0; i<j; i++) {
		if(i<k) {
			qspline_int += y[i]*(x[i+1]-x[i])+0.5*b[i]*(x[i+1]*x[i+1]-x[i]*x[i])
				-b[i]*x[i]*(x[i+1]-x[i])+
				1.0/3*c[i]*(x[i+1]*x[i+1]*x[i+1]-x[i]*x[i]*x[i])
				+c[i]*x[i]*x[i]*(x[i+1]-x[i])-c[i]*x[i]*(x[i+1]*x[i+1]-x[i]*x[i]);
			}
		else {
			qspline_int += y[i]*(z-x[i])+0.5*b[i]*(z*z-x[i]*x[i])
				-b[i]*x[i]*(z-x[i])+1.0/3*c[i]*(z*z*z-x[i]*x[i]*x[i])
				+c[i]*x[i]*x[i]*(z-x[i])-c[i]*x[i]*(z*z-x[i]*x[i]);
			}
	}
	
	return qspline_int;
}

void qspline_free(qspline *qs) {
	double* x = qs->x;
	double* y = qs->y;
	double* b = qs->b;
	double* c = qs->c;

	free(x);free(y);free(b);free(c);free(qs);	
}
