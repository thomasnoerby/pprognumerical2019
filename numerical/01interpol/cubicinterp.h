#include<stdlib.h>
#include<stdio.h>
#include<assert.h>

typedef struct {int n; double *x, *y, *b, *c, *d;} cubspline;
cubspline* cubspline_alloc(int n, double* x, double* y);
double cubspline_evaluate(cubspline* qs, double z);
double cubspline_derivative(cubspline *qs, double z);
double cubspline_integral(cubspline *qs, double z);
void cubspline_free(cubspline *qs);
