#include<stdlib.h>
#include"cubicinterp.h"
#include<stdio.h>
#include<assert.h>
#include<math.h>

cubspline* cubspline_alloc(int n, double* x, double* y)
{
	cubspline* qs = (cubspline*) malloc(sizeof(cubspline));
	qs->n = n;
	qs->x = (double*) malloc(n*sizeof(double));
	qs->y = (double*) malloc(n*sizeof(double));
	qs->b = (double*) malloc(n*sizeof(double));
	qs->c = (double*) malloc((n-1)*sizeof(double));
	qs->d = (double*) malloc((n-1)*sizeof(double));

	for(int i = 0; i<n; i++) {
		qs->x[i] = x[i];
		qs->y[i] = y[i];
	}

	double p[n-1], h[n-1];
	for(int i = 0; i<n-1; i++) {
		h[i] = x[i+1]-x[i];
		assert(h[i]>0); 
		p[i] = (y[i+1]-y[i])/h[i];
	}

	double D[n], Q[n-1], B[n];
	D[0] = 2;
	Q[0] = 1;
	for(int i = 0; i<n-2; i++) {
		B[i+1] = 3*(p[i]+p[i+1]*h[i] / h[i+1]);
		Q[i+1] = h[i] / h[i+1];
		D[i+1] = 2*h[i] / h[i+1]+2;
	}
	D[n-1] = 2;
	B[0] = 3*p[0];
	B[n-1] = 3*p[n-2];

	for(int i = 1; i<n; i++) {
		B[i] -= B[i-1]/D[i-1];	
		D[i] -= Q[i-1]/D[i-1];
	}	

	qs->b[n-1] = B[n-1]/D[n-1];
	for(int i = n-2; i>=0; i--) {
		qs->b[i] = (B[i]-Q[i]*qs->b[i+1])/D[i];
	}

	for(int i = 0; i<n-1; i++) {
		qs->c[i] = (-2*qs->b[i]-qs->b[i+1]+3*p[i])/h[i];
		qs->d[i] = (qs->b[i]+qs->b[i+1]-2*p[i]) / (h[i]*h[i]);
	}

	return qs;	
}

double cubspline_evaluate(cubspline* qs, double z){
	double* x = qs->x;
	double* y = qs->y;
	double* b = qs->b;
	double* c = qs->c;
	double* d = qs->d;
	int n = qs->n; 
	assert(z>=x[0] && z<= x[n-1]);


	int i = 0, j = n-1; 
	while(j-i>1) {
		int mid = (i+j)/2;
		if(z>x[mid]) {i = mid;}
		else {j = mid;}
	}

	double diff = z-x[i];
	double spline = y[i]+b[i]*diff+c[i]*diff*diff+d[i]*diff*diff*diff;

	return spline;
}

double cubspline_derivative(cubspline *qs, double z) {
	double* x = qs->x;
	double* b = qs->b;
	double* c = qs->c;
	double* d = qs->d;
	int n = qs->n; 

	int i = 0, j = n-1; 
	while(j-i>1) {
		int mid = (j+i)/2;
		if(z>x[mid]) {i = mid;}
		else {j = mid;}
	}

	double diff = z-x[i];
	double cubspline_der = b[i]+c[i]*2*diff+d[i]*3*diff*diff;

	return cubspline_der;
}

double cubspline_integral(cubspline *qs, double z) {
	int k = 0, j = qs->n-1; 
	while(j-k>1) {
		int mid = (k+j)/2;
		if(z>qs->x[mid]) {k = mid;}
		else {j = mid;}
	}

	double cubspline_int = 0;
	double xi, xip, diff, diff2, diff3, diff4, xi2, xi3, yi, bi, ci, di;
	for(int i = 0; i<j; i++) {
		if(i<k) {
			xip = qs->x[i+1];
		}
		else {
			xip = z;
		}
		xi = qs->x[i];
		xi2 = xi*xi;
		xi3 = xi2*xi;
		yi = qs->y[i];
		bi = qs->b[i];
		ci = qs->c[i];
		di = qs->d[i];
		diff = xip-xi;
		diff2 = xip*xip-xi2;
		diff3 = pow(xip,3)-xi3;
		diff4 = pow(xip,4)-xi3*xi;

		cubspline_int += yi*diff+0.5*bi*diff2 - bi*xi*diff;
		cubspline_int += 1.0/3*ci*diff3 + ci*xi2*diff - ci*xi*diff2;
		cubspline_int += di*(1.0/4*diff4-xi3*diff+3.0/2*xi2*diff2-xi*diff3);
	}
	return cubspline_int;
}

void cubspline_free(cubspline *qs) {
	free(qs->x);
	free(qs->y);
	free(qs->b);
	free(qs->c);
	free(qs->d);
	free(qs);
}
