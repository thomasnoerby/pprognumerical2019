#include"lineq.h"
#include<gsl/gsl_blas.h>
#include<stdio.h>

int main() {
	printf("Testing A.1: qr_gs_decomp\n");
	int n = 10;
	int m = 8;

	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_matrix* check_A = gsl_matrix_alloc(n,m);
	gsl_matrix* R = gsl_matrix_alloc(m,m);
	gsl_matrix* check_I = gsl_matrix_alloc(m,m);
	
	printf("Creating random matrix A with dimensions %d x %d:\n",n,m);
	random_mat(A);
	printer(A);

	printf("Performing QR-decomp of A\n");
	qr_gs_decomp(A,R);

	printf("R should be upper triangular. It is: \n");
	printer(R);

	printf("Q^TQ should equal the identity. It is: \n");
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,check_I);
	printer(check_I);

	printf("QR should equal A. QR is:\n");
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,check_A);
	printer(check_A);
	
	gsl_matrix_free(A);
	gsl_matrix_free(check_A);
	gsl_matrix_free(R);
	gsl_matrix_free(check_I);
	printf("Test completed\n");


	printf("\nTesting A.2: qr_gs_solve\n");
	m = n;
	A = gsl_matrix_alloc(n,m);
	R = gsl_matrix_alloc(m,m);
	gsl_matrix* memA = gsl_matrix_alloc(n,m);
	gsl_vector* b = gsl_vector_alloc(m);
	gsl_vector* x = gsl_vector_alloc(m);

	printf("Creating random square matrix A with dimensions %d x %d:\n",n,m);
	random_mat(A);
	gsl_matrix_memcpy(memA,A);
	printer(A);

	printf("Creating random vector b of dimension %d x 1:\n",m);
	random_vec(b);
	printer_vec(b);

	printf("Performing QR-decomp of A\n");
	qr_gs_decomp(A,R);

	printf("Performing QR-solve\n");
	qr_gs_solve(A,R,b,x);

	printf("Ax should equal b. It is: \n");
	gsl_blas_dgemv(CblasNoTrans,1.0,memA,x,0,b);
	printer_vec(b);

	gsl_vector_free(b);
	gsl_vector_free(x);
	printf("Test completed\n");	

	
	printf("\nTesting B: Matrix inverse\n");
	gsl_matrix* B = gsl_matrix_alloc(n,m);	
	check_I = gsl_matrix_alloc(n,m);
	printf("The random square matrix A from the previous test is used. The QR-decomp is already done.\n");
	
	printf("Calculating inverse of A:\n");
	qr_gs_inverse(A,R,B);
	printer(B);
	
	printf("AB should equal the identity. It is\n");
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,memA,B,0.0,check_I);
	printer(check_I);

	gsl_matrix_free(check_I);
	gsl_matrix_free(A);
	gsl_matrix_free(memA);
	gsl_matrix_free(R);
	gsl_matrix_free(B);
	printf("Test completed\n");
	return 0;
}

