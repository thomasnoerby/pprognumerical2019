#include"monte.h"
#include<stdio.h>
#include<math.h>

double singular_function(gsl_vector* v) 
{
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	double z = gsl_vector_get(v,2);
	
	return 1.0/(1-cos(x)*cos(y)*cos(z))/(M_PI*M_PI*M_PI);
}

double func(gsl_vector* v)
{
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);

	return y*y*x;
}

int main() 
{
	int n1 = 3, n2 = 2;
	int N = 10000000;
	double err1 = 0, err2 = 0;
	gsl_vector* a1 = gsl_vector_alloc(n1);
	gsl_vector* b1 = gsl_vector_alloc(n1);
	gsl_vector* a2 = gsl_vector_alloc(n2);
	gsl_vector* b2 = gsl_vector_alloc(n2);
	
	gsl_vector_set_all(a1,0);
	gsl_vector_set_all(b1,M_PI);
	gsl_vector_set_all(a2,0);
	gsl_vector_set(b2,0,1);
	gsl_vector_set(b2,1,2);
	
	printf("Calculating difficult singular integral with %d points\n",N);
	double result =	plain_monte_carlo(singular_function,a1,b1,N,&err1);
	printf("The result is %g with an error of %g\n",result,err1);
	printf("The result should be 1.3932039\n");

	int N2 = 1e+5;
	int i;
	for(i = 1; i<N2; i+=100) {
		result = plain_monte_carlo(singular_function,a1,b1,i,&err1);
		result = plain_monte_carlo(func,a2,b2,i,&err2);
		fprintf(stderr,"%d %g %g\n",i,err1,err2);
	}	
	printf("\nCalculation of integral of f(x,y) = x*y*y from (0,0) to (1,2) has been calculated with %d points\n",i);
	printf("The result is %g with an error of %g\n",result,err2);
	printf("It should be 4/3\n");

	printf("Error convergence rates as a function of points can be seen in plot.svg\n");

	gsl_vector_free(a1);
	gsl_vector_free(b1);	
	gsl_vector_free(a1);
	gsl_vector_free(b1);
	
	return 0;
}
