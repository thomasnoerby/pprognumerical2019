#include"monte.h"
#include<math.h>
#include<stdio.h>
#include<time.h>

double plain_monte_carlo(double f(gsl_vector* v), gsl_vector* a, gsl_vector* b, int N, double* err) 
{
	int n = a->size;
	gsl_vector* rand_vec = gsl_vector_alloc(n);
	
	int i;
	double volume = 1;
	double interval, ai, bi;
	for(i = 0; i<n; i++) {
		ai = gsl_vector_get(a,i);
		bi = gsl_vector_get(b,i);
		interval = bi-ai;	
		volume *= interval;
	}

	double sum = 0, prod = 0;
	double f_val;
	unsigned int seed = time(NULL);
	for(i = 0; i<N; i++) {
		random_vector(rand_vec,a,b,&seed);
		f_val = f(rand_vec);
		sum += f_val;
		prod += f_val*f_val;	
	}

	double mean = sum/N;
	double sigma = sqrt(prod/N-mean*mean);
	double stat_err = sigma/sqrt(N);

	double integral = mean*volume;
	*err = stat_err*volume;
	
	gsl_vector_free(rand_vec);

	return integral;
}

void random_vector(gsl_vector* rand_vec, gsl_vector* a, gsl_vector* b, unsigned int* seed)
{
	int n = rand_vec->size;
	double ai, bi, interval, rand, rand_val;
	for(int i = 0; i<n; i++) {
		ai = gsl_vector_get(a,i);
		bi = gsl_vector_get(b,i);	
		interval = bi-ai;	
		rand = (double) rand_r(seed)/RAND_MAX;
		rand_val = rand*interval+ai;
		gsl_vector_set(rand_vec,i,rand_val);
	}
}
