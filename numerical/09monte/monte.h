#include<gsl/gsl_vector.h>
double plain_monte_carlo(double f(gsl_vector* v), gsl_vector* a, gsl_vector* b, int N, double* err);
void random_vector(gsl_vector* rand_vec, gsl_vector* a, gsl_vector* b, unsigned int* seed);
//void random_vector(gsl_vector* rand_vec, gsl_vector* a, gsl_vector* b);
