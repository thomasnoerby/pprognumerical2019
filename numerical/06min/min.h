#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
void newton(
	double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
	gsl_vector* xstart,
	double eps);
void newton_with_broyden(
	double f(gsl_vector* x, gsl_vector* df),
	gsl_vector* xstart,
	double eps);
void backsub(const gsl_matrix* U, gsl_vector* c);
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x);
void printer_vec(gsl_vector* vec);
void printer(gsl_matrix* matter);
double dot_product(double alpha, gsl_vector* a, gsl_vector* b);
void switch_sign(gsl_vector* fx);
double norm(gsl_vector* fx);
