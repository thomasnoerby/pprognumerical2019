#include"min.h"
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<stdio.h>

double rosenbrock(gsl_vector* x, gsl_vector* df, gsl_matrix* H);
double himmelblau(gsl_vector* x, gsl_vector* df, gsl_matrix* H);
double rosenbrock_broyden(gsl_vector* x, gsl_vector* df);
double himmelblau_broyden(gsl_vector* x, gsl_vector* df);

double minfunction(gsl_vector* x, gsl_vector* df);

int main() {
	int n = 2;
	double eps = 1e-6;
	gsl_vector* himmel = gsl_vector_alloc(n);
	gsl_vector* rosen = gsl_vector_alloc(n);
	gsl_vector* fitparams = gsl_vector_alloc(3);

	gsl_vector_set(himmel,0,10);
	gsl_vector_set(himmel,1,20);

	printf("Exercise A\n");
	printf("Solving Himmelblau with start point (%g,%g)\n",gsl_vector_get(himmel,0),gsl_vector_get(himmel,1));
	newton(himmelblau,himmel,eps);
	double x_val = gsl_vector_get(himmel,0);
	double y_val = gsl_vector_get(himmel,1);
	double f = (x_val*x_val+y_val-11)*(x_val*x_val+y_val-11)
		+(x_val+y_val*y_val-7)*(x_val+y_val*y_val-7);
	printf("Converged at point (%g,%g) with a function value of f = %g\n",x_val,y_val,f);
	
	gsl_vector_set(rosen,0,-3);
	gsl_vector_set(rosen,1,-10);

	printf("\nSolving Rosenbrock with start point (%g,%g)\n",gsl_vector_get(rosen,0),gsl_vector_get(rosen,1));
	newton(rosenbrock,rosen,eps);
	x_val = gsl_vector_get(rosen,0);
	y_val = gsl_vector_get(rosen,1);
	f = (1-x_val)*(1-x_val)+100*(y_val-x_val*x_val)*(y_val-x_val*x_val);
	printf("Converged at point (%g,%g) with a function value of f = %g\n",x_val,y_val,f);


	printf("\nExercise B - Broyden's update\n");
	gsl_vector_set(himmel,0,10);
	gsl_vector_set(himmel,1,20);

	printf("\nSolving Himmelblau without analytical Hessian with start point (%g,%g)\n",gsl_vector_get(himmel,0),gsl_vector_get(himmel,1));
	newton_with_broyden(himmelblau_broyden,himmel,eps);
	x_val = gsl_vector_get(himmel,0);
	y_val = gsl_vector_get(himmel,1);
	f = (x_val*x_val+y_val-11)*(x_val*x_val+y_val-11)
		+(x_val+y_val*y_val-7)*(x_val+y_val*y_val-7);
	printf("Converged at point (%g,%g) with a function value of f = %g\n",x_val,y_val,f);

	gsl_vector_set(rosen,0,-3);
	gsl_vector_set(rosen,1,-10);

	printf("\nSolving Rosenbrock without analytical Hessian with start point (%g,%g)\n",gsl_vector_get(rosen,0),gsl_vector_get(rosen,1));
	newton_with_broyden(rosenbrock_broyden,rosen,eps);
	x_val = gsl_vector_get(rosen,0);
	y_val = gsl_vector_get(rosen,1);
	f = (1-x_val)*(1-x_val)+100*(y_val-x_val*x_val)*(y_val-x_val*x_val);
	printf("Converged at point (%g,%g) with a function value of f = %g\n",x_val,y_val,f);

	printf("\nFrom the root-finding exercise, the number of iterations and function calls are (with the same starting points):\n");
	printf("Himmelblau: 10 iterations and 20 function calls (analytic Jacobian), 10 iterations and 20 function calls (numerical Jacobian)\n");
	printf("Rosenbrock: 436 iterations and 3298 function calls (analytic Jacobian), 2393 iterations and 30566 function calls (numerical Jacobian)\n");
	


	printf("\nLeast squares fitting problem\n");
	gsl_vector_set(fitparams,0,5);
	gsl_vector_set(fitparams,1,10);
	gsl_vector_set(fitparams,2,0.5);

	printf("Solving fit with start point (A = %g, T = %g, B = %g)\n",gsl_vector_get(fitparams,0),gsl_vector_get(fitparams,1),gsl_vector_get(fitparams,2));
	newton_with_broyden(minfunction,fitparams,eps);
	printf("Converged at point (A = %g, T = %g, B = %g)\n",gsl_vector_get(fitparams,0),gsl_vector_get(fitparams,1),gsl_vector_get(fitparams,2));
	printf("A plot of this can be seen in plot.svg\n");

	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	for(int i = 0; i<N; i++) {
		fprintf(stderr,"%g %g %g\n",t[i],y[i],e[i]);
	}

	double A = gsl_vector_get(fitparams,0);
	double T = gsl_vector_get(fitparams,1);
	double B = gsl_vector_get(fitparams,2);
	double val;
	FILE* fp = fopen("fit.data","w+");
	for(double x = 0; x<10; x+=0.1) {
		val = A*exp(-x/T)+B;
		fprintf(fp,"%g %g\n",x,val);
	}
	fclose(fp);

	gsl_vector_free(rosen);
	gsl_vector_free(himmel);
	gsl_vector_free(fitparams);
	return 0;
}

double rosenbrock(gsl_vector* x, gsl_vector* df, gsl_matrix* H)
{
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);
	
	double dfdx = 2*x_val-2-200*(y_val-x_val*x_val)*2*x_val;
	double dfdy = 200*(y_val-x_val*x_val); 

	gsl_vector_set(df,0,dfdx);
	gsl_vector_set(df,1,dfdy);

	double df2dx2 = 2-400*y_val+1200*x_val*x_val;
	double df2dxdy = -400*x_val;
	double df2dydx = -400*x_val;
	double df2dy2 = 200; 

	gsl_matrix_set(H,0,0,df2dx2);
	gsl_matrix_set(H,0,1,df2dxdy);
	gsl_matrix_set(H,1,0,df2dydx);
	gsl_matrix_set(H,1,1,df2dy2);

	double f = (1-x_val)*(1-x_val)+100*(y_val-x_val*x_val)*(y_val-x_val*x_val);
	return f;
}

double himmelblau(gsl_vector* x, gsl_vector* df, gsl_matrix* H)
{
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);

	double dfdx = 4*x_val*(x_val*x_val+y_val-11)+2*(x_val+y_val*y_val-7);
	double dfdy = 2*(x_val*x_val+y_val-11)+4*y_val*(x_val+y_val*y_val-7);

	gsl_vector_set(df,0,dfdx);
	gsl_vector_set(df,1,dfdy);

	double df2dx2 = 4*y_val-44+12*x_val*x_val+2;
	double df2dxdy = 4*x_val+4*y_val;
	double df2dydx = 4*x_val+4*y_val;
	double df2dy2 = 2+4*x_val-28+12*y_val*y_val;

	gsl_matrix_set(H,0,0,df2dx2);
	gsl_matrix_set(H,0,1,df2dxdy);
	gsl_matrix_set(H,1,0,df2dydx);
	gsl_matrix_set(H,1,1,df2dy2);

	double f = (x_val*x_val+y_val-11)*(x_val*x_val+y_val-11)
		+(x_val+y_val*y_val-7)*(x_val+y_val*y_val-7);

	return f;
}

double rosenbrock_broyden(gsl_vector* x, gsl_vector* df)
{
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);
	
	double dfdx = 2*x_val-2-200*(y_val-x_val*x_val)*2*x_val;
	double dfdy = 200*(y_val-x_val*x_val); 

	gsl_vector_set(df,0,dfdx);
	gsl_vector_set(df,1,dfdy);

	double f = (1-x_val)*(1-x_val)+100*(y_val-x_val*x_val)*(y_val-x_val*x_val);
	return f;
}

double himmelblau_broyden(gsl_vector* x, gsl_vector* df)
{
	double x_val = gsl_vector_get(x,0);
	double y_val = gsl_vector_get(x,1);

	double dfdx = 4*x_val*(x_val*x_val+y_val-11)+2*(x_val+y_val*y_val-7);
	double dfdy = 2*(x_val*x_val+y_val-11)+4*y_val*(x_val+y_val*y_val-7);

	gsl_vector_set(df,0,dfdx);
	gsl_vector_set(df,1,dfdy);

	double f = (x_val*x_val+y_val-11)*(x_val*x_val+y_val-11)
		+(x_val+y_val*y_val-7)*(x_val+y_val*y_val-7);

	return f;
}

double minfunction(gsl_vector* x, gsl_vector* df)
{
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	double f = 0, dfA = 0, dfT = 0, dfB = 0;
	double func, diff, err;
	for(int i = 0; i<N; i++) {
		func = A*exp(-t[i]/T)+B;
		diff = (func-y[i]);
		err = e[i]*e[i];
		f += diff*diff/err;
		dfA += 2*diff*exp(-t[i]/T)/err;
		dfT += 2*diff*A*t[i]/T/T*exp(-t[i]/T)/err;
		dfB += 2*diff/err;
	}

	gsl_vector_set(df,0,dfA);
	gsl_vector_set(df,1,dfT);
	gsl_vector_set(df,2,dfB);

	return f;
}

