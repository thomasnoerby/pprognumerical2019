#include"min.h"
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<assert.h>

void newton_with_broyden(
	double f(gsl_vector* x, gsl_vector* df),
	gsl_vector* xstart,
	double eps)
{
	int n = xstart->size;
	gsl_matrix* B = gsl_matrix_alloc(n,n);
	gsl_vector* df = gsl_vector_alloc(n); 
	gsl_vector* old_df = gsl_vector_alloc(n); 
	gsl_vector* x = gsl_vector_alloc(n); 
	gsl_vector* delta_x = gsl_vector_alloc(n); 
	gsl_vector* s = gsl_vector_alloc(n); 
	gsl_vector* y = gsl_vector_alloc(n);	
	gsl_vector* u = gsl_vector_alloc(n);
	gsl_vector* c = gsl_vector_alloc(n);
	gsl_matrix* delta_B = gsl_matrix_alloc(n,n);
	gsl_vector_memcpy(x,xstart);
	
	gsl_matrix_set_identity(B);

	int iter = 0; 
	int iter_func = 0;
	double length; 
	double lambda; 
	double alpha = 1e-3; 
	double func_val;
	double cur_func_val;
	double armijo;
	double sum, bij, pj, yj, si, sty, ui, dfsi, dfi, ci, sj, dbij;
	int i, j;
	
	do {
		iter++;
		func_val = f(x,df);
		iter_func++;
		gsl_vector_memcpy(old_df,df);

		// delta_x
		sum = 0;
		for(i = 0; i<n; i++) {
			for(j = 0; j<n; j++) {
				bij = gsl_matrix_get(B,i,j);		
				pj = gsl_vector_get(df,j);
				sum -= bij*pj;
			}
			gsl_vector_set(delta_x,i,sum);
			sum = 0;
		}
		
		lambda = 2;
		do {
			lambda/= 2;
			gsl_vector_set_zero(s);
			gsl_blas_daxpy(lambda,delta_x,s);
			gsl_vector_memcpy(xstart,s);
			gsl_vector_add(xstart,x);
			cur_func_val = f(xstart,df); // df = delta_phi (x+s)
			iter_func++;
			armijo = func_val+dot_product(alpha,s,old_df);
		}
		while(cur_func_val >= armijo && lambda > 1.0/pow(2,14));
		if(lambda == 1.0/pow(2,14)) { // from Jens Jensen
			gsl_matrix_set_identity(B);
		}
		gsl_vector_add(x,s); // x+s

		// y
		for(i = 0; i<n; i++) {
			dfsi = gsl_vector_get(df,i);
			dfi = gsl_vector_get(old_df,i);
			gsl_vector_set(y,i,dfsi-dfi);
		}

		// u	
		for(i = 0; i<n; i++) {
			for(j = 0; j<n; j++) {
				bij = gsl_matrix_get(B,i,j);
				yj = gsl_vector_get(y,j);
				sum -= bij*yj;
			}
			si = gsl_vector_get(s,i);
			sum += si;
			gsl_vector_set(u,i,sum);
			sum = 0;
		}
		
		// c
		sty = dot_product(1.0,s,y);
		if(sty > 1e-6) {
			for(i = 0; i<n; i++) {
				ui = gsl_vector_get(u,i);		
				ui/=sty;
				gsl_vector_set(c,i,ui);
			}
		}

		// delta_B
		for(i = 0; i<n; i++) {
			ci = gsl_vector_get(c,i);
			for(j = 0; j<n; j++) {
				sj = gsl_vector_get(s,j);
				gsl_matrix_set(delta_B,i,j,ci*sj);
			}
		}

		// new B
		for(i = 0; i<n; i++) {
			for(j = 0; j<n; j++) {
				bij = gsl_matrix_get(B,i,j);
				dbij = gsl_matrix_get(delta_B,i,j);
				gsl_matrix_set(B,i,j,bij+dbij);
			}
		}
		
		length = norm(df);
	}
	while(length >= eps);

	gsl_vector_memcpy(xstart,x);
	printf("Gradient\n");
	printer_vec(df);
	printf("The function converged after %d iterations and %d function calls\n",iter,iter_func);

	gsl_matrix_free(B);
	gsl_vector_free(df); 
	gsl_vector_free(old_df); 
	gsl_vector_free(x); 
	gsl_vector_free(delta_x); 
	gsl_vector_free(s); 
	gsl_vector_free(y);	
	gsl_vector_free(u);
	gsl_vector_free(c);
	gsl_matrix_free(delta_B);
}

void newton(
	double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
	gsl_vector* xstart,
	double eps)
{
	int n = xstart->size;
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector* delta_x = gsl_vector_alloc(n);
	gsl_vector* s = gsl_vector_alloc(n);
	gsl_matrix* H = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* old_df = gsl_vector_alloc(n);
	gsl_vector_memcpy(x,xstart);

	int iter = 0;
	int iter_func = 0;
	double length;
	double lambda;
	double alpha = 1e-3;
	double func_val;
	double cur_func_val;
	double armijo;
	
	do {
		iter++;
		func_val = f(x,df,H);
		iter_func++;
		gsl_vector_memcpy(old_df,df);

		switch_sign(df);

		qr_gs_decomp(H,R);
		qr_gs_solve(H,R,df,delta_x);

		switch_sign(df);

		lambda = 2;
		do {
			lambda/= 2;
			gsl_vector_set_zero(s);
			gsl_blas_daxpy(lambda,delta_x,s);
			gsl_vector_memcpy(xstart,s);
			gsl_vector_add(xstart,x);
			cur_func_val = f(xstart,df,H);
			iter_func++;
			armijo = func_val+dot_product(alpha,s,old_df);
		}
		while(cur_func_val >= armijo && lambda > 1.0/64);
		gsl_vector_add(x,s);
		length = norm(df);
	}
	while(length >= eps);

	gsl_vector_memcpy(xstart,x);
	printf("Gradient\n");
	printer_vec(df);
	printf("The function converged after %d iterations and %d function calls\n",iter,iter_func);

	gsl_vector_free(old_df);
	gsl_vector_free(df);
	gsl_vector_free(x);
	gsl_vector_free(delta_x);
	gsl_vector_free(s);
	gsl_matrix_free(H);
	gsl_matrix_free(R);
}

double dot_product(double alpha, gsl_vector* a, gsl_vector* b) 
{
	int n = a->size;
	int m = b->size;
	assert(n==m);
	double sum = 0;
	for(int i = 0; i<n; i++) {
		sum += gsl_vector_get(a,i)*gsl_vector_get(b,i);
	}
	sum *= alpha;
	
	return sum;	
}

void switch_sign(gsl_vector* fx)
{
	int n = fx->size;
	for(int i = 0; i<n; i++) {
		double fxi = gsl_vector_get(fx,i);
		gsl_vector_set(fx,i,-fxi);
	}
}

double norm(gsl_vector* fx)
{
	int n = fx->size;
	double length = 0;
	for(int i = 0; i<n; i++) {
		double fxi = gsl_vector_get(fx,i);
		length += fxi*fxi; 	
	}
	length = sqrt(length);
	
	return length;
}


// taken from Dmitri's slides
void backsub(const gsl_matrix* U, gsl_vector* c) {
	for(int i = c->size-1; i>=0; i--) {
		double s = gsl_vector_get(c,i);
		for(int k = i+1; k < c->size; k++) {
			s -= gsl_matrix_get(U,i,k)*gsl_vector_get(c,k);
		}

		gsl_vector_set(c,i,s/gsl_matrix_get(U,i,i));
	}	
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R) {
	for(int i = 0; i < A->size2; i++) {
		double inner_prod = 0;
		for(int j = 0; j < A->size1; j++) {
			double aji=gsl_matrix_get(A,j,i);
			inner_prod += aji*aji;
		}
		double Rii = sqrt(inner_prod);
		gsl_matrix_set(R,i,i,Rii);

		for(int j = 0; j < A->size1; j++) {
			double aji = gsl_matrix_get(A,j,i)/Rii;
			gsl_matrix_set(A,j,i,aji);
		}

		for(int j = i+1; j< A->size2; j++) {
			double qitaj = 0;
			for(int k = 0; k<A->size1; k++) {
				qitaj += gsl_matrix_get(A,k,i)*gsl_matrix_get(A,k,j);
			}			
			gsl_matrix_set(R,i,j,qitaj);

			for(int k = 0; k< A->size1; k++) {
				double akj = gsl_matrix_get(A,k,j)
					- gsl_matrix_get(A,k,i)*gsl_matrix_get(R,i,j);	
				gsl_matrix_set(A,k,j,akj);
			}
		}
	}
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x) {
	gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x);

	for(int i = 0; i < Q->size2; i++) {
		double sum = 0;
		for(int j = 0; j < Q->size1; j++) {
			sum += gsl_matrix_get(Q,j,i)*gsl_vector_get(b,j);
		}
		gsl_vector_set(x,i,sum);
	}

	backsub(R,x);
}

void printer_vec(gsl_vector* vec) {
	int n = vec->size;
	for(int i = 0; i<n; i++) {
		printf("x_%d %6.3g\n",i,gsl_vector_get(vec,i));
	}

}

void printer(gsl_matrix* matter) {
	int n = matter->size1, m = matter->size2;
	for(int i = 0; i<n; i++) {
		for(int j = 0; j<m; j++) {
			printf("%8.2f ",gsl_matrix_get(matter,i,j));
		}
		printf("\n");
	}

}

