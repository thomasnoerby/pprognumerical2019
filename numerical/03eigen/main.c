#include<stdio.h>
#include<gsl/gsl_blas.h>
#include"jacobi_eig.h"

int main() {
	int n = 4;
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* A_cpy = gsl_matrix_alloc(n,n);
	gsl_matrix* B = gsl_matrix_alloc(n,n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	gsl_matrix* D = gsl_matrix_alloc(n,n);
	gsl_vector* eig = gsl_vector_alloc(n);
	random_mat(A);
	gsl_matrix_memcpy(A_cpy,A);
	gsl_matrix_memcpy(B,A);

	printf("Exercise A\n");
	printf("Initial A matrix:\n");
	printer(A);

	printf("Applying rotation ...\n");

	jacobi_rotation(A, eig, V);
	printf("A after rotation:\n");
	printer(A);

	printf("Eigenvector matrix:\n");
	printer(V);
	
	printf("Eigenvalues:\n");
	printer_vec(eig);

	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A_cpy,0.0,D);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,D,V,0.0,A_cpy);

	printf("V^T A V = D:\n");
	printer(A_cpy);

	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_matrix_free(D);
	gsl_matrix_free(A_cpy);
	gsl_vector_free(eig);	


	printf("Exercise B\n");
	int no_of_eigvals = 1;
	eig = gsl_vector_alloc(no_of_eigvals);	

	printf("A matrix:\n");
	printer(B);	

	printf("Eigenvalue by eigenvalue (%d eigenvalues):\n",no_of_eigvals);
	jacobi_eigenvalue_by_eigenvalue(B,eig,no_of_eigvals);
	printf("A after rotation:\n");
	printer(B);
	
	printf("Eigenvalues:\n");
	printer_vec(eig);

	printf("\nThe largest value can be obtained first by changing the formula,\nsuch that the angle formula is \nphi = 1.0/2*atan2(-2.0*Apq / (App-Aqq)) instead of \nphi = 1.0/2*atan2(2.0*Apq / (Aqq-App)).\n");

	printf("Dimension scaling and time comparisons can be seen in the svg files\n");

	gsl_matrix_free(B);
	gsl_vector_free(eig);

	return 0;
}
