#include<stdio.h>
#include<stdlib.h>
#include"jacobi_eig.h"
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

int main(int argc, char** argv) {

	int n;
	if(argc>1) {
		n = atoi(argv[1]);
	}

	int no_of_eig = 1;

	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_vector* eig = gsl_vector_alloc(no_of_eig);

	random_mat(A);

	jacobi_eigenvalue_by_eigenvalue(A,eig,no_of_eig);	

	gsl_matrix_free(A);
	gsl_vector_free(eig);
}
