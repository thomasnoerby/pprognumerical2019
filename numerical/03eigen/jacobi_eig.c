#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<math.h>
#include<assert.h>
#include"jacobi_eig.h"

void jacobi_rotation(gsl_matrix* A, gsl_vector* eig, gsl_matrix* V) {
	int n = A->size1, m = A->size2;
	assert(n == m);
	
	gsl_matrix_set_identity(V);

	double aii;
	int i;
	for(i = 0; i<n; i++) {
		aii = gsl_matrix_get(A,i,i);
		gsl_vector_set(eig,i,aii);
	}

	int status = 1, iter = 0;
	double api, aqi, app, aqq, apq, phi, aip, aiq, vip, viq;
	double new_api, new_aqi, new_app, new_aqq, new_aip, new_aiq, new_vip, new_viq;
	double c, s, cc, ss, cs;
	int p, q;

	while(status != 0) {
		iter++;
		status = 0;
		for(p = 0; p<n; p++) {
			for(q = p+1; q<n; q++) {
				app = gsl_vector_get(eig,p);
				aqq = gsl_vector_get(eig,q);
				apq = gsl_matrix_get(A,p,q);
				phi = 1.0/2*atan2(2.0*apq,(aqq-app));

				c = cos(phi);
				s = sin(phi);
				cc = c*c;
				ss = s*s;
				cs = c*s;

				new_app = cc*app-2*cs*apq+ss*aqq;
				new_aqq = ss*app+2*cs*apq+cc*aqq;

				gsl_vector_set(eig,p,new_app);
				gsl_vector_set(eig,q,new_aqq);
				gsl_matrix_set(A,p,q,0.0);

				if(app != new_app || aqq != new_aqq) {status = 1;}

				for(i = 0; i<p;i++) {
					aip = gsl_matrix_get(A,i,p);
					aiq = gsl_matrix_get(A,i,q);

					new_aip = c*aip-s*aiq;
					new_aiq = s*aip+c*aiq;

					gsl_matrix_set(A,i,p,new_aip);
					gsl_matrix_set(A,i,q,new_aiq);
				}
				for(i = p+1; i<q;i++) {
					api = gsl_matrix_get(A,p,i);
					aiq = gsl_matrix_get(A,i,q);

					new_api = c*api-s*aiq;
					new_aiq = s*api+c*aiq;

					gsl_matrix_set(A,p,i,new_api);
					gsl_matrix_set(A,i,q,new_aiq);
				}
				for(i = q+1; i<m;i++) {
					api = gsl_matrix_get(A,p,i);
					aqi = gsl_matrix_get(A,q,i);

					new_api = c*api-s*aqi;
					new_aqi = s*api+c*aqi;

					gsl_matrix_set(A,p,i,new_api);
					gsl_matrix_set(A,q,i,new_aqi);
				}
				for(i = 0; i<n; i++) {
					vip = gsl_matrix_get(V,i,p);
					viq = gsl_matrix_get(V,i,q);
					
					new_vip = c*vip-s*viq;
					new_viq = s*vip+c*viq;

					gsl_matrix_set(V,i,p,new_vip);
					gsl_matrix_set(V,i,q,new_viq);
				}

			}
		}
	}
	
	printf("Converged after %d sweeps\n",iter);
}

void jacobi_eigenvalue_by_eigenvalue(gsl_matrix* A, gsl_vector* eig, int no_of_eigenvalues) {
	int n = A->size1, m = A->size2;
	assert(n == m);
	int n_eig = eig->size;
	assert(n_eig == no_of_eigenvalues);
	assert(no_of_eigenvalues <= n);

	int status = 0, iter = 0;
	double api, aqi, app, aqq, apq, phi, aip, aiq;
	double new_api, new_aqi, new_app, new_aqq, new_aip, new_aiq;
	double c, s, cc, ss, cs;

	if(no_of_eigenvalues == n) {
		no_of_eigenvalues = n-1; // the same number of eigenvalues, but prevents stuck
	}

	int p = 0, q, i;

	while(status == 0) {
		iter++;
		for(q = p+1; q<n; q++) {
			app = gsl_matrix_get(A,p,p);
			aqq = gsl_matrix_get(A,q,q);
			apq = gsl_matrix_get(A,p,q);
			phi = 1.0/2*atan2(2.0*apq,(aqq-app));

			c = cos(phi);
			s = sin(phi);
			cc = c*c;
			ss = s*s;
			cs = c*s;

			new_app = cc*app-2*cs*apq+ss*aqq;
			new_aqq = ss*app+2*cs*apq+cc*aqq;

			gsl_matrix_set(A,p,p,new_app);
			gsl_matrix_set(A,q,q,new_aqq);
			gsl_matrix_set(A,p,q,0.0);

			status = (app == new_app && aqq == new_aqq);
			if((status) && (p<no_of_eigenvalues-1)) {
				p++; status = 0; }
			for(i = 0; i<p;i++) {
				aip = gsl_matrix_get(A,i,p);
				aiq = gsl_matrix_get(A,i,q);

				new_aip = c*aip-s*aiq;
				new_aiq = s*aip+c*aiq;

				gsl_matrix_set(A,i,p,new_aip);
				gsl_matrix_set(A,i,q,new_aiq);
			}
			for(i = p+1; i<q;i++) {
				api = gsl_matrix_get(A,p,i);
				aiq = gsl_matrix_get(A,i,q);

				new_api = c*api-s*aiq;
				new_aiq = s*api+c*aiq;

				gsl_matrix_set(A,p,i,new_api);
				gsl_matrix_set(A,i,q,new_aiq);
			}
			for(i = q+1; i<m;i++) {
				api = gsl_matrix_get(A,p,i);
				aqi = gsl_matrix_get(A,q,i);

				new_api = c*api-s*aqi;
				new_aqi = s*api+c*aqi;

				gsl_matrix_set(A,p,i,new_api);
				gsl_matrix_set(A,q,i,new_aqi);
			}
		}
	}

	double aii;
	for(i = 0; i<n_eig; i++) {
		aii = gsl_matrix_get(A,i,i);
		gsl_vector_set(eig,i,aii);
	}
	
	printf("Converged after %d sweeps\n",iter);
} 

void printer(gsl_matrix* matter) {
	int n = matter->size1, m = matter->size2;
	for(int i = 0; i<n; i++) {
		for(int j = 0; j<m; j++) {
			printf("%8.4f ",gsl_matrix_get(matter,i,j));
		}
		printf("\n");
	}

}

void printer_vec(gsl_vector* vec) {
	int n = vec->size;
	for(int i = 0; i<n; i++) {
		printf("x_%d %6.3g\n",i,gsl_vector_get(vec,i));
	}

}

void random_mat(gsl_matrix* matter) {
	int n = matter->size1, m = matter->size2;
	for(int i = 0; i<n; i++) {
		for(int j = 0; j<m; j++) {
			if(j<i) {
				double val = gsl_matrix_get(matter,j,i);
				gsl_matrix_set(matter,i,j,val);
			}
			else {
				double randomnr =(double) rand()/RAND_MAX;
				gsl_matrix_set(matter,i,j,randomnr);
			}
		}
	}
}
