#include<gsl/gsl_matrix.h>
void jacobi_rotation(gsl_matrix* A, gsl_vector* eig, gsl_matrix* V);
void jacobi_eigenvalue_by_eigenvalue(gsl_matrix* A, gsl_vector* eig, int no_of_eigenvalues);
void printer(gsl_matrix* matter); 
void printer_vec(gsl_vector* vec);
void random_mat(gsl_matrix* matter);
