#include<stdio.h>
#include<stdlib.h>
#include"jacobi_eig.h"
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

int main(int argc, char** argv) {

	int n;
	if(argc>1) {
		n = atoi(argv[1]);
	}

	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	gsl_vector* eig = gsl_vector_alloc(n);

	random_mat(A);

	jacobi_rotation(A,eig,V);	

	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(eig);
}
