#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_sf_airy.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_eigen.h>
int main() {
	printf("Exercise 1\n");
	for(double x=-16; x<6; x+=0.01) {
		double y = gsl_sf_airy_Ai(x,0);
		double z = gsl_sf_airy_Bi(x,0);
		fprintf(stderr,"%g \t %g \t %g\n",x,y,z);
	}

	printf("\nExercise 2\n");
	int n = 3;

	double a_data[] = {6.13, -2.90, 5.86, // array holds data of the matrix
			8.08, -6.31, -3.89,
			-4.36, 1, 0.19};

	double b_data[] = {6.23, 5.37, 2.29}; // array holds data of the vector

	gsl_matrix_view a = gsl_matrix_view_array(a_data,n,n); // the array is viewed like a matrix
	gsl_vector_view b = gsl_vector_view_array(b_data,n);

	gsl_matrix * c = gsl_matrix_alloc(n,n); // copy of the a matrix for result checking
	gsl_matrix_memcpy(c,&a.matrix);

	gsl_vector *x = gsl_vector_alloc(n); // vector to hold the result

	int s; // holds the sign of the permutation (from LU decomposition)
	gsl_permutation * p = gsl_permutation_alloc(n); // to hold the permutation
	gsl_linalg_LU_decomp(&a.matrix, p, &s); //&a.matrix: the adress of a converted to a matrix

	gsl_linalg_LU_solve(&a.matrix, p, &b.vector,x);
	printf("The x vector solving the system of linear equations is:\n");
	gsl_vector_fprintf(stdout,x,"%g");

	gsl_vector * result = gsl_vector_alloc(n);

	gsl_blas_dgemv(CblasNoTrans,1,c,x,0,result);

	printf("Checking that the solution is correct. The result is:\n");
	gsl_vector_fprintf(stdout,result,"%g");
	printf("The result should be:\n");
	gsl_vector_fprintf(stdout,&b.vector,"%g");

	gsl_permutation_free(p);
	gsl_matrix_free(c);
	gsl_vector_free(x);
	gsl_vector_free(result);

	printf("\nExercise 3\n");
	printf("Based on an example found\n");
	int size = 4;
	double d[] = {1.0, 1.0/2, 1.0/3, 1.0/4,
			1.0/2, 1.0/3, 1.0/4, 1.0/5,
			1.0/3, 1.0/4, 1.0/5, 1.0/6,
			1.0/4, 1.0/5, 1.0/6, 1.0/7};
	gsl_matrix_view hilbert = gsl_matrix_view_array(d,size,size);

	gsl_vector* eval = gsl_vector_alloc(size);
	gsl_matrix* evec = gsl_matrix_alloc(size,size);
	gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc(size); // creates a workspace to calculate eigvals and eigvecs

	gsl_eigen_symmv(&hilbert.matrix, eval, evec, w); // calculates eigvals and eigvecs
	gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_ABS_ASC); // sorts the eigvals and eigvecs simultaneously in ascending order of magnitude

	for(int i = 0; i<size; i++)
	{
		double eigval_i = gsl_vector_get(eval,i);
		gsl_vector_view eigvec_i = gsl_matrix_column(evec,i);

		printf("Eigenvalue = %g\n",eigval_i);
		printf("Corresponding eigenvector: \n");
		gsl_vector_fprintf(stdout, &eigvec_i.vector, "%g");
	}
	gsl_eigen_symmv_free(w);
	gsl_vector_free(eval);
	gsl_matrix_free(evec);

	return 0;
}

