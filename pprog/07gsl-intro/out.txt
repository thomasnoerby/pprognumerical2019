Exercise 1

Exercise 2
The x vector solving the system of linear equations is:
-1.1379
-2.83303
0.851459
Checking that the solution is correct. The result is:
6.23
5.37
2.29
The result should be:
6.23
5.37
2.29

Exercise 3
Based on an example found
Eigenvalue = 9.67023e-05
Corresponding eigenvector: 
-0.0291933
0.328712
-0.791411
0.514553
Eigenvalue = 0.00673827
Corresponding eigenvector: 
-0.179186
0.741918
-0.100228
-0.638283
Eigenvalue = 0.169141
Corresponding eigenvector: 
0.582076
-0.370502
-0.509579
-0.514048
Eigenvalue = 1.50021
Corresponding eigenvector: 
0.792608
0.451923
0.322416
0.252161
