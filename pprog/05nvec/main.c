#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#include "assert.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if (double_equal(vi, value)) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_equal ...\n");
	nvector *p = nvector_alloc(n);
	nvector *q = nvector_alloc(n);
	nvector_set(p,0,1);
	nvector_set(q,0,1.0001);
	if(!nvector_equal(p,q)) {
		printf("test passed\n");
	}
	else {
		printf("test failed\n");
	}

	nvector_free(p);
	nvector_free(q);

	printf("\nmain: testing nvector_add ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}
	nvector_add(a, b);
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);

	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");

	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);

	printf("\nmain: testing nvector_sub ...\n");

	a = nvector_alloc(n);
	b = nvector_alloc(n);
	c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x - y);
	}
	nvector_sub(a,b);
	if(nvector_equal(c,a)) {printf("test passed\n");}
	else {printf("test failed\n");}

	nvector_free(a);
	nvector_free(b);
	nvector_free(c);

	printf("\nmain: testing set_zero ...\n");
	nvector *d = nvector_alloc(n);
	nvector_set_zero(d);
	int setZero = 1;
	for(int i = 0; i<n; i++) {
		if(nvector_get(d,i) != 0) {setZero = 0;}
	}
	if(setZero) {printf("test passed\n");}
	else {printf("test failed\n");}

	printf("\nmain: testing dot_product ...\n");
	nvector *e = nvector_alloc(n);
	nvector_set_zero(d);
	double res = 0;
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(d, i, x);
		nvector_set(e, i, y);
		res += x*y;
	}
	if(double_equal( nvector_dot_product(d,e),res)) {printf("test passed\n");}
	else {printf("test failed\n");}

	nvector_free(d);
	nvector_free(e);

	printf("\nmain: testing scale ...\n");
	d = nvector_alloc(n);
	e = nvector_alloc(n);
	double scale = 2;
	for(int i = 0; i<n; i++) {
		double x = RND;
		nvector_set(d,i,x);
		nvector_set(e,i,2*x);
	}

	nvector_scale(d,scale);
	if(nvector_equal(e, d)) {
		printf("test passed\n");
	}
	else {
		printf("test failed\n");
	}


	nvector_free(d);
	nvector_free(e);


	return 0;
}
