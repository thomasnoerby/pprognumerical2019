#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>
#include<assert.h>
#include<math.h>
nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v);}

void nvector_set(nvector* v, int i, double value){
	assert( 0 <= i && i < (*v).size);
	(*v).data[i]=value;
}

double nvector_get(nvector* v, int i){
	assert( 0 <= i && i < (*v).size);
	return (*v).data[i];
}

double nvector_dot_product(nvector* u, nvector* v) {
	int uSize = (*u).size;
	int vSize = (*v).size;
	assert(uSize == vSize);

	double result = 0.0;
	for(int i = 0; i<uSize; i++) {
		result += nvector_get(u,i)*nvector_get(v,i);
	}
	return result;
}

int double_equal(double a, double b) {
	double acc = 1e-6;
	double diff = fabs(a-b);
	int absPrec = diff < acc;

	double absA = fabs(a);
	double absB = fabs(b);
	int relPrec;
	if(absA+absB == 0) {
		printf("Division by zero in relative precision, it is automatically set to false.\n");
		relPrec = 0;
	}
	else {
		relPrec = (diff/(absA+absB)) < (acc/2);
	}
	int result = absPrec | relPrec;
	return result;
}

void nvector_print(char* s, nvector* v) {
	int vSize = (*v).size;
	printf("%s [",s);
	for(int i = 0; i<vSize; i++)
	{
		if(i<vSize-1)
		{
			printf("%g, ",nvector_get(v,i));
		}
		else {
			printf("%g]\n",nvector_get(v,i));
		}
	}
}

void nvector_set_zero(nvector* v) {
	for(int i = 0; i<(*v).size; i++) {
		nvector_set(v,i,0.0);
	}
}

int nvector_equal (nvector* a, nvector* b) {
	int aSize = (*a).size;
	int bSize = (*b).size;
	int sizeEquality = (aSize == bSize);

	int dataEquality = 1;
	for(int i = 0; i<aSize; i++) {
		if(!double_equal(nvector_get(a,i),nvector_get(b,i))) {dataEquality = 0;} // implement relative and absolute prec instead.
	}
	return sizeEquality & dataEquality;
}

void nvector_add(nvector* a, nvector* b) {
	int aSize = (*a).size;
	int bSize = (*b).size;
	assert(aSize == bSize);

	for(int i = 0; i<aSize; i++) {
		nvector_set(a, i, nvector_get(a,i)+nvector_get(b,i));
	}
}

void nvector_sub(nvector* a, nvector* b) {
	int aSize = (*a).size;
	int bSize = (*b).size;
	assert(aSize == bSize);

	for(int i = 0; i<aSize; i++) {
		nvector_set(a, i, nvector_get(a,i) - nvector_get(b,i));
	}
}

void nvector_scale(nvector* a, double x) {
	int aSize = (*a).size;
	for(int i = 0; i<aSize; i++) {
		nvector_set(a, i, x * nvector_get(a, i));
	}
}
/* ... */
