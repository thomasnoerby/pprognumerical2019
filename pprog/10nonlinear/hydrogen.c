#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>

struct parameters {double rmin; double rmax;};

int ode(double r, const double y[], double dydt[], void* params) {
	double ener = *(double*) params;
	dydt[0] = y[1];
	dydt[1] = -2*(ener+1/r)*y[0];
	return GSL_SUCCESS;
}

double ode_solution(double rmin, double rmax, double energy) {
	gsl_odeiv2_system sys;
	sys.function = ode;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (double*)&energy;

	gsl_odeiv2_driver *driver;
	double hstart = 0.1, abs = 1e-6, eps = 1e-6;	
	driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45, hstart, abs, eps);

	double y[] = {rmin-rmin*rmin,1-2*rmin};
	gsl_odeiv2_driver_apply(driver, &rmin, rmax, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int root_aux_fun(const gsl_vector* z, void* params, gsl_vector* f) {
	double energy = gsl_vector_get(z,0);

	struct parameters par = *(struct parameters*) params;
	double rmin = par.rmin;
	double rmax = par.rmax;

	double mismatch = ode_solution(rmin,rmax,energy);
	gsl_vector_set(f,0,mismatch);

	return GSL_SUCCESS;
}

double hydrogen(double rmin, double rmax, double energy_start) {
	gsl_multiroot_function F;
	F.f = root_aux_fun;
	F.n = 1;
	struct parameters params;
	params.rmin = rmin;
	params.rmax = rmax;
	F.params = (void*) &params;

	gsl_vector* x0 = gsl_vector_alloc(F.n);
	gsl_vector_set(x0,0,energy_start);

	gsl_multiroot_fsolver* S;
	S = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids, F.n);
	gsl_multiroot_fsolver_set(S,&F,x0);

	int flag, iter = 0;
	do {
		iter++;
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,1e-12);
	}
	while (flag == GSL_CONTINUE);

	double eps_result = gsl_vector_get(S->x,0);

	gsl_vector_free(x0);
	gsl_multiroot_fsolver_free(S);

	return eps_result;
}
