#include<stdio.h>
#include<math.h>

double root(double, double);
double hydrogen(double, double, double);
double ode_solution(double,double,double);

double exact(double r) {
	double result = r*exp(-r);
	return result;
}

int main(){
	double x_start = 0;
	double y_start = -2;
	root(x_start, y_start);

	FILE *fp = fopen("hydrogen.data","w+");
	double rmax = 8;
	double rmin = 0.01;
	double energy_start = -1;
	double energy_lowest_root = hydrogen(rmin,rmax,energy_start);	
	fprintf(fp,"%g\n",energy_lowest_root);
	fclose(fp);	

	FILE *fp2 = fopen("hydrogen-plateu.data","w+");
	for(double r=0.0001;r<0.2;r+=0.01) {
	double res_energy = hydrogen(r,rmax,energy_start);
		fprintf(fp2,"%g %g\n",r,res_energy);
	}
	fclose(fp2);

	rmin = 0.01;
	FILE* fp3 = fopen("hydrogen.data","w+");
	for(double r = rmin; r<=rmax; r+= 0.01) {
		double result = ode_solution(rmin,r,energy_lowest_root);
		double exact_result = exact(r);
		fprintf(fp3,"%g %g %g\n", r, result, exact_result);
	} 
	fclose(fp3);
}
