#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#define TYPE gsl_multiroot_fsolver_hybrids

double rosenbrock(double x, double y) {
	double res = (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
	return res;
}

int root_equation (const gsl_vector* z, void* params, gsl_vector* f)
{
	double x0 = gsl_vector_get(z,0);
	double y0 = gsl_vector_get(z,1);
	double fx = 2*(-1+x0-200*(y0-x0*x0)*x0);
	double fy = 200*(y0-x0*x0);
	
	gsl_vector_set(f,0,fx);
	gsl_vector_set(f,1,fy);

	return GSL_SUCCESS;
}

int root(double x_start, double y_start){
	gsl_multiroot_function F;
	F.f = root_equation;
	F.n = 2;
	F.params = NULL;

	gsl_vector* x = gsl_vector_alloc(F.n);
	gsl_vector_set(x,0,x_start);
	gsl_vector_set(x,1,y_start);

	gsl_multiroot_fsolver* S;
	S = gsl_multiroot_fsolver_alloc(TYPE,F.n);
	gsl_multiroot_fsolver_set(S,&F,x);

	int flag, iter = 0;
	double x_step, y_step, f_value;
	do {
		x_step = gsl_vector_get((*S).x,0);
		y_step = gsl_vector_get((*S).x,1);
		f_value = rosenbrock(x_step, y_step); 	
		fprintf(stderr,"%g %g %g\n",x_step,y_step,f_value);

		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual((*S).f, 1e-12);
		iter++;
	}while(flag == GSL_CONTINUE);

	double result_x = gsl_vector_get((*S).x,0);
	double result_y = gsl_vector_get((*S).x,1);

	printf("The extremum point of the Rosenbrock function is at f(%g,%g)\n"
		,result_x,result_y);
	printf("The algorithm converged after %i iterations\n",iter);

	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(x);
	return GSL_SUCCESS;
}
