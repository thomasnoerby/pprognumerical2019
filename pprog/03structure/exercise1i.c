#include<stdio.h>
#include<limits.h>
void exercise1i() {
	printf("Exercise 1i\n");
	int i = 1;

	while(i+1>i) {
		i++;
	}
	printf("Max int while = %i\n",i);

	for(i = 1; i+1>i; i++) {;}
	printf("Max int for loop = %i\n",i);

	i = 1;
	do(i++);
	while(i+1>i);
	printf("Max int do while = %i\n", i);

	printf("INT_MAX = %i\n",INT_MAX);
}
