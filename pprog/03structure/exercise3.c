#include<stdio.h>
#include<math.h>

int equal(double a, double b, double tau, double epsilon) {
	double absDiff = fabs(a-b);
	int absPrec = absDiff < tau;
	int relPrec = absDiff/(fabs(a)+fabs(b)) < epsilon/2;
	int result = absPrec | relPrec;
	return result;
}
