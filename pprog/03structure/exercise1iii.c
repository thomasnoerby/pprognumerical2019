#include<float.h>
#include<stdio.h>

void exercise1iii() {
	printf("\nExercise1iii\n");

	double x = 1;
	while(1+x!=1) {x/=2;}
	x*=2;
	printf("while, double = %g\n",x);

	float z = 1;
	while(1+z!=1) {z/=2;}
	z*=2;
	printf("while, float = %g\n",z);

	long double y = 1;
	while(1+y!=1) {y/=2;}
	y*=2;
	printf("while, long double = %Lg\n",y);

	for(x=1; 1+x!=1; x/=2) {}
	x*=2;
	printf("for, double = %g\n",x);

	for(z=1; 1+z!=1; z/=2) {}
	z*=2;
	printf("for, float = %g\n",z);

	for(y=1; 1+y!=1; y/=2) {}
	y*=2;
	printf("for, long double = %Lg\n",y);

	do(x/=2);
	while(1+x!=1);
	x*=2;
	printf("dowhile, double = %g\n",x);

	do(z/=2);
	while(1+z!=1);
	z*=2;
	printf("dowhile, float = %g\n",z);

	do(y/=2);
	while(1+y!=1);
	y*=2;
	printf("dowhile, long double = %Lg\n",y);

	printf("Double machine epsilon = %g\n",DBL_EPSILON);
	printf("Float machine epsilon = %g\n",FLT_EPSILON);
	printf("Long double machine epsilon = %Lg\n",LDBL_EPSILON);
}

