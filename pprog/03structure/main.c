#include<stdio.h>
#include<limits.h>
#include<float.h>

void exercise1i();
void exercise1ii();
void exercise1iii();
void exercise2();
void exercise3(); // the equal function is in here
void exercise4(); // the name_digit function is in here

int main() {
	exercise1i();
	exercise1ii();
	exercise1iii();
	exercise2();
}
