#include<stdio.h>
#include<math.h>
#include<limits.h>

void exercise2() {
	printf("\nExercise 2i\n");

	int max = INT_MAX/3;

	float sum_up_float = 0.0f;
	float sum_down_float = 0.0f;
	for(int i = 1; i<max+1;i++) {
		sum_up_float += 1.0f/i;
		sum_down_float += 1.0f/(max-i+1);
	}
	printf("sum_up_float = %f\n",sum_up_float);
	printf("sum_down_float = %f\n",sum_down_float);

	printf("\nExercise 2ii\n");
	printf("The difference is caused by the machine epsilon for floats. This is not very small, so there will be round off errors.\n");
	printf("This has an effect on the order of adding the numbers.\n");

	printf("\nExercise 2iii\n");
	printf("The sum appears to converge, but does not (very slow conversion). The result is the same for INT_MAX/3, INT_MAX/2 and INT_MAX/3*2.\n");

	printf("\nExercise 2iv\n");

	double sum_up_double = 0.0;
	double sum_down_double = 0.0;
	for(int i = 1; i<max+1; i++) {
		sum_up_double += 1.0/i;
		sum_down_double += 1.0/(max-i+1);
	}
	printf("sum_up_double = %g\n", sum_up_double);
	printf("sum_down_double = %g\n", sum_down_double);
}
