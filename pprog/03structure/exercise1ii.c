#include<stdio.h>
#include<limits.h>
void exercise1ii() {
	printf("\nExercerise 1ii\n");
	int i = 1;

	while(i-1<i) {
		i--;
	}	
	printf("Min int while = %i\n",i);

	for(i=1; i-1<i; i--){;}
	printf("Min int for loop = %i\n", i);

	i = 1;
	do(i--);
	while(i-1<i);
	printf("Min int do while = %i\n",i);

	printf("INT_MIN = %i\n", INT_MIN);
}
