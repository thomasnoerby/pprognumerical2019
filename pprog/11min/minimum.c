#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#define TYPE gsl_multimin_fdfminimizer_vector_bfgs2

double rosenbrock(double x, double y) {
	double res = (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
	return res;
}

double min_equation (const gsl_vector* z, void* params){
	double x = gsl_vector_get(z,0);
	double y = gsl_vector_get(z,1);
	double result = (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
	
	return result;
}

void min_grad(const gsl_vector *v, void *params, gsl_vector *df) {
	double x0 = gsl_vector_get(v,0);
	double y0 = gsl_vector_get(v,1);

	double fx = 2*(-1+x0-200*(y0-x0*x0)*x0);
	double fy = 200*(y0-x0*x0);

	gsl_vector_set(df,0,fx);
	gsl_vector_set(df,1,fy);
}

void min_fdf(const gsl_vector* x, void* params, double* f, gsl_vector* df) {
	*f = min_equation(x, params);
	min_grad(x,params,df);
}

int minimum(double x_start, double y_start){
	gsl_multimin_function_fdf F;
	F.f = min_equation;
	F.df = min_grad;
	F.fdf = min_fdf;
	F.n = 2;
	F.params = NULL;

	gsl_vector* x = gsl_vector_alloc(F.n);
	gsl_vector_set(x,0,x_start);
	gsl_vector_set(x,1,y_start);

	gsl_multimin_fdfminimizer* S;
	S = gsl_multimin_fdfminimizer_alloc(TYPE,F.n);
	double step_size = 0.1;
	double tol = 1e-4;
	gsl_multimin_fdfminimizer_set(S,&F,x,step_size,tol);

	int flag, iter = 0;
	double x_step, y_step, f_value, epsabs = 1e-6;
	do {
		x_step = gsl_vector_get((*S).x,0);
		y_step = gsl_vector_get((*S).x,1);
		f_value = rosenbrock(x_step, y_step); 	
		fprintf(stderr,"%g %g %g\n",x_step,y_step,f_value);

		gsl_multimin_fdfminimizer_iterate(S);

		flag = gsl_multimin_test_gradient(S->gradient,epsabs);
		iter++;
	}while(flag == GSL_CONTINUE && iter<100);

	double result_x = gsl_vector_get((*S).x,0);
	double result_y = gsl_vector_get((*S).x,1);

	printf("The minimum point of the Rosenbrock function is at f(%g,%g)\n"
		,result_x,result_y);
	printf("The algorithm converged after %i iterations\n",iter);

	gsl_multimin_fdfminimizer_free(S);
	gsl_vector_free(x);
	return GSL_SUCCESS;
}
