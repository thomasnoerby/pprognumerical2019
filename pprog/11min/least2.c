#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>
#include<math.h>
#define TYPE gsl_multimin_fminimizer_nmsimplex2

struct expdata {int n; double* time; double* activity; double* error;};

double function(const gsl_vector* x, void* params) {
	double a = gsl_vector_get(x,0);
	double b = gsl_vector_get(x,1);
	double t = gsl_vector_get(x,2);

	struct expdata data = *(struct expdata*) params;
	double result = 0;
	for(int i = 0; i<data.n;i++){
		double time = data.time[i];
		double activity = data.activity[i];
		double error = data.error[i];
		result += pow(a*exp(-time/t)+b-activity,2)/error/error;
	}

	return result;
}

int least2(int argc, char** argv) {
	assert(argc>=2);

	int n = atoi(argv[1]);
	double time[n], activity[n], error[n];

	for(int i=0; i<n; i++) {
		scanf("%lg %lg %lg", time+i, activity+i, error+i);
	}

	int dim = 3;
	struct expdata data;
	data.n = n;
	data.time = time;
	data.activity = activity;
	data.error = error;

	gsl_multimin_function F;
	F.f = function;
	F.n = dim;
	F.params = (void*) &data;

	gsl_multimin_fminimizer* M;
	M = gsl_multimin_fminimizer_alloc(TYPE,dim);

	gsl_vector* start = gsl_vector_alloc(dim);
	gsl_vector* step = gsl_vector_alloc(dim);	

	gsl_vector_set(start,0,5);
	gsl_vector_set(start,1,1);
	gsl_vector_set(start,2,2);

	gsl_vector_set(step,0,1);
	gsl_vector_set(step,1,1);
	gsl_vector_set(step,2,0.2);

	gsl_multimin_fminimizer_set(M,&F,start,step);

	int iter = 0, flag;
	double size;

	FILE* fp = fopen("least2.out.txt","w+");
	do{
		iter++;
		flag = gsl_multimin_fminimizer_iterate(M);
		if(flag) break;

		size = gsl_multimin_fminimizer_size(M);
		flag = gsl_multimin_test_size(size,1e-3);

		if(flag == GSL_SUCCESS) {
			fprintf(fp,"Least squares converged to a minimum at \n");
		}

		fprintf(fp,"iter = %i A=%g B = %g T = %g\n",iter, gsl_vector_get(M->x, 0),
			gsl_vector_get(M->x,1), gsl_vector_get(M->x,2));
	}
	while(flag == GSL_CONTINUE && iter < 100);


	double a = gsl_vector_get(M->x,0);
	double b = gsl_vector_get(M->x,1);
	double t = gsl_vector_get(M->x,2);

	fprintf(fp,"Activity background lifetime\n");
	fprintf(fp,"%8g %8g %8g\n",a,b,t);
	fclose(fp);

	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(M);
	return GSL_SUCCESS;
}
