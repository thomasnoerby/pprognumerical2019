#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<math.h>

int ode(double t, const double y[], double dydt[], void* params)
{
	dydt[0] = 1 + y[0]*y[0];
	return GSL_SUCCESS;
}

double tangent_solver(double t)
{
	if(t < 0) return tangent_solver(t+M_PI);
	if(t > M_PI/2) return -tangent_solver(-fmod(t,M_PI)); 

	gsl_odeiv2_system sys;
	sys.function = ode;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	gsl_odeiv2_driver* driver;
	double hstart = 0.1, abs = 1e-6, eps = 1e-6;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45, hstart, abs, eps);

	double t0 = 0;
	double y[] = {0.0};

	gsl_odeiv2_driver_apply(driver, &t0, t, y);

	gsl_odeiv2_driver_free(driver);
	
	return y[0];
}
