#include<stdio.h>
#include<math.h>

double tangent_solver(double);

int main()
{
	for(double i = -M_PI; i<= M_PI; i+=0.05){
		double result = tangent_solver(i);
		printf("%g %g %g\n", i, result, tan(i));
	}
	return 0;
}
