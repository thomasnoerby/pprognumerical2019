#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<pthread.h>

struct params {unsigned int seed; int N,Nin;};

void* func_thread(void* parameters) {
	struct params* pars = (struct params*) parameters;
	unsigned int* seed = &(pars->seed);

	for(int i = 0; i<pars->N;i++)
	{
		double x = (double)rand_r(seed)/RAND_MAX;
		double y = (double)rand_r(seed)/RAND_MAX;
		if( x*x+y*y<=1 ) {
			pars->Nin += 1;
		}
	}
	return NULL;
}

double run_pthreads(int n){

	struct params pars1 = {.seed = 1, .N = n/2, .Nin = 0};
	struct params pars2 = {.seed = 2, .N = n/2, .Nin = 0}; 

	pthread_t thread;
	pthread_create(&thread,NULL,func_thread, (void*) &pars1);
	
	func_thread((void*) &pars2);
	pthread_join(thread,NULL);

	double pi_estimate = 4.0*(pars1.Nin+pars2.Nin)/(pars1.N+pars2.N);
	fprintf(stderr,"run_pthread: The estimate of pi using %i iterations using pthread is = %g\n",n,pi_estimate);

	return pi_estimate;
}

double run_openmp(int n){

	struct params pars1 = {.seed = 13, .N = n/4, .Nin = 0};
	struct params pars2 = {.seed = 42, .N = n/4, .Nin = 0}; 
	struct params pars3 = {.seed = 33, .N = n/4, .Nin = 0};
	struct params pars4 = {.seed = 17, .N = n/4, .Nin = 0};

	#pragma omp parallel sections
	{
	#pragma omp section
		{
			func_thread((void*) &pars1);
		}
	#pragma omp section
		{
			func_thread((void*) &pars2);
		}
	#pragma omp section
		{
			func_thread((void*) &pars3);
		}
	#pragma omp section
		{
			func_thread((void*) &pars4);
		}
	}

	double pi_estimate = 4.0*(pars1.Nin+pars2.Nin+pars3.Nin+pars4.Nin)/(pars1.N+pars2.N+pars3.N+pars4.N);
	fprintf(stderr,"The estimate of pi using %i iterations with OpenMP is = %g\n",n, pi_estimate);
	return pi_estimate;
}

int main(int argc,char** argv){
	int n = argc>1 ? (int) atof(argv[1]):1e5;
	int prog = argc>2 ? atoi(argv[2]):0;
	double result;
	if(prog==0){
		fprintf(stderr,"running pthreads...\n");
		result=run_pthreads(n);
		}
	else{
		fprintf(stderr,"running openmp...\n");
		result=run_openmp(n);
	}
	printf("%i %g\n",n,result);

return 0;
}

/*

	int N_conv = 0, Nin_conv = 0;
	#pragma omp parallel reduction(+:N_conv,Nin_conv) 
	{
		#pragma omp for ordered
		for(int i = 0; i<n;i++)
		{
			double x = rand_r(&seed)/1e8;
			seed += 1;
			double y = rand_r(&seed)/1e8;
			seed += 1;
			if((x<=1.0) & (y<=1.0)) {
				N_conv += 1;
			}
			if((x<=1.0) & (y<=1.0) & (x*x+y*y<=1)) {
				Nin_conv += 1;
			}
			#pragma omp ordered
			if(N_conv!=0) {
				fprintf(stderr,"%i %g\n",i,4.0*Nin_conv/N_conv);
			}
		}
	}
*/
