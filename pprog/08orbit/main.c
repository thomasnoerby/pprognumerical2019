#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>
#include<stdio.h>
#include<math.h>
double orbit(double);
double gen_rel_orbit(double, double, double);

int main() {
	for(double x = 0; x<3.1; x+=0.1) {
		double logistic = 1/(exp(-x)+1);
		fprintf(stdout,"%g %g %g\n", x, orbit(x), logistic);
	} 
	

	for(double x = 0;x<10*M_PI+0.1; x+=0.1) {
		double circular = gen_rel_orbit(x,0,0);
		double ellipsoid = gen_rel_orbit(x,0,-0.5);
		double relativistic = gen_rel_orbit(x,0.01,-0.5);
		fprintf(stderr, "%g %g %g %g\n", x, circular, ellipsoid, relativistic);
	}
	
	return 0;
}
