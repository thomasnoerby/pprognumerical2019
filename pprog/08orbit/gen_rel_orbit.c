#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
int gen_rel_ode_orbit(double t, const double y[], double dydt[], void* params)
{
	double correction = *(double *) params;
	dydt[0] = y[1];
	dydt[1] = 1-y[0]+correction*y[0]*y[0];
	return GSL_SUCCESS;
}

double gen_rel_orbit(double t, double correction, double prime_start)
{
	gsl_odeiv2_system sys;
	sys.function = gen_rel_ode_orbit;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = &correction;

	gsl_odeiv2_driver *driver;
	double hstart = 0.1, abs = 1e-6, eps = 1e-6;
	driver  =gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45, hstart, abs, eps);

	double t0 = 0;
	double y[] = {1, prime_start};
	gsl_odeiv2_driver_apply(driver, &t0, t, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}
