#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<math.h>
double integrand(double t, void* params) {
	double f = log(t)/sqrt(t); 
	return f;
}

double lnx() {
	int limit = 100;
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);
	
	gsl_function F;
	F.function = integrand;
	F.params = NULL;

	double result, error, acc = 1e-6, eps = 1e-6;
	int flag = gsl_integration_qags(&F, 0, 1, acc, eps, limit, w, &result, &error);

	gsl_integration_workspace_free(w);
	
	if(flag != GSL_SUCCESS) return NAN;
	return result;
}
