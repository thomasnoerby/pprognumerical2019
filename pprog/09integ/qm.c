#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<math.h>
double qm_denominator(double x, void* params) {	
	double alpha = *(double*) params;
	double f = exp(-alpha*x*x);
	return f;
}

double qm_numerator(double x, void* params) {
	double alpha = *(double*) params;
	double f = exp(-alpha*x*x)*(-alpha*alpha*x*x/2+alpha/2+x*x/2);
	return f;
}

double qm_expectation(double alpha) {
	int limit = 100;
	gsl_integration_workspace* w_den = gsl_integration_workspace_alloc(limit);
	gsl_integration_workspace* w_num = gsl_integration_workspace_alloc(limit);

	gsl_function F_den;
	F_den.function = qm_denominator;
	F_den.params = (void*) &alpha;

	gsl_function F_num;
	F_num.function = qm_numerator;
	F_num.params = (void*) &alpha;

	double result_den, error_den, abs = 1e-6, eps = 1e-6;
	int flag_den = gsl_integration_qagi
		(&F_den, abs, eps, limit, w_den, &result_den, &error_den);

	double result_num, error_num;
	int flag_num = gsl_integration_qagi
		(&F_num, abs, eps, limit, w_num, &result_num, &error_num);

	gsl_integration_workspace_free(w_den);
	gsl_integration_workspace_free(w_num);

	if((flag_den!=GSL_SUCCESS) | (flag_num!=GSL_SUCCESS)) return NAN;

	double result = result_num/result_den;
	return result;
}
