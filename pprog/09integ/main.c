#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<math.h>
double lnx();
double qm_expectation(double);

int main() {
	printf("Exercise 1\n");
	printf("Result of integration: %g\n",lnx());

	printf("Exercise 2\n");
	for(double alpha = 0.1; alpha<2; alpha+=0.01) {
		double res = qm_expectation(alpha);
		fprintf(stderr,"%g %g\n",alpha,res);
	}
	return 0;
}
