#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

double errfunc(double);

int main(int argc, char** argv) {
	assert(argc > 3);
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double stepsize = atof(argv[3]);
	
	for(double x = a; x<b; x+=stepsize) {
		double result = errfunc(x);
		printf("%g %g\n",x,result);
	}

	return 0;
}
