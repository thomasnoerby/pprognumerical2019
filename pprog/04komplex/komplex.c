#include<stdio.h>
#include"komplex.h"
#include<math.h>

void komplex_print (char *s, komplex a) {
	printf("%s (%g,%g)\n", s, a.re, a.im);
}

komplex komplex_new (double x, double y) {
	komplex z = {x, y};
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = {a.re + b.re, a.im + b.im};
	return result;
}

komplex komplex_sub (komplex a, komplex b) {
	komplex result = {a.re - b.re, a.im - b.im};
	return result;
}

int komplex_equal (komplex a, komplex b, double acc, double eps) {
	double reDiv = fabs(a.re)+fabs(b.re);
	double imDiv = fabs(a.im)+fabs(b.im);
	int absPrecReal = fabs(a.re-b.re) < acc;
	int absPrecImag = fabs(a.im-b.im) < acc;
	int relPrecReal = reDiv != 0.0 ? ( fabs(a.re-b.re) / reDiv ) < (eps/2) : 0; // false if division by zero
	int relPrecImag = imDiv != 0.0 ? ( fabs(a.im-b.im) / imDiv ) < (eps/2) : 0; // false if division by zero
	int result = (absPrecReal & absPrecImag) | (relPrecReal & relPrecImag);
	return result;
}

komplex komplex_mul (komplex a, komplex b) {
	double real = a.re*b.re - a.im*b.im;
	double imag = a.re*b.im + a.im*b.re;
	komplex result = {real, imag};
	return result;
}

komplex komplex_div (komplex a, komplex b) {
	double div = pow(b.re,2) + pow(b.im,2);
	if(div == 0.0) {
	printf("Error: Division by zero. Returning first argument.\n");
	return a;
	}
	double real = (a.re*b.re+a.im*b.im) / div;
	double imag = (a.im*b.re-a.re*b.im) / div;
	komplex result = {real, imag};
	return result;
}

double komplex_abs (komplex z) {
	double abs = sqrt(pow(z.re,2)+pow(z.im,2));
	return abs;
}


