#include"komplex.h"
#include"stdio.h"
#include"math.h"
#define TINY 1e-6

int main() {
	komplex a = {1,2}, b = {3, 4};
	komplex c = {1,2};

	printf("testing komplex add... \n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a =", a);
	komplex_print("b=", b);
	komplex_print("a+b should = ", R);
	komplex_print("a+b actually = ", r);


	printf("\ntesting komplex sub... \n");
	komplex s = komplex_sub(a,b);
	komplex S = {-2, -2};
	komplex_print("a = ", a);
	komplex_print("b = ", b);
	komplex_print("a-b should = ", S);
	komplex_print("a+b actually = ", s);

	/* the following is optional */

	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'add' passed \n");
	else
		printf("test 'add' failed\n");

	printf("\ntesting komplex equal... \n");
	int t = komplex_equal(a,c,TINY,TINY);
	int f = komplex_equal(a,b,TINY, TINY);
	printf("a == c should be 1 and is %i \n", t);
	printf("a == b should be 0 and is %i \n", f);

	printf("\ntesting komplex mul ...\n");
	komplex m = komplex_mul(a,b);
	komplex M = {-5,10};
	if(komplex_equal(m,M,TINY,TINY))
		printf("test 'mul' passed \n");
	else
		printf("test 'mul' failed \n");

	printf("\ntesting komplex div ...\n");
	komplex d = komplex_div(a,b);
	komplex D = {0.44,0.08};
	printf("Testing division by zero safe\n");
	if(komplex_equal(d,D,TINY,TINY))
		printf("1. test of 'div' passed \n");
	else
		printf("1. test of 'div' failed \n");

	komplex oneZero = komplex_new(1.0,0.0);
	komplex zeroZero = komplex_new(0.0,0.0);
	komplex g = komplex_div(oneZero,zeroZero);
	if(komplex_equal(g,oneZero,TINY,TINY))
		printf("2. test of 'div' passed\n"); // if both imag parts are zero, division by zero error, so first argument is returned.
	else
		printf("2. test of 'div' failed\n");

	printf("\ntesting komplex abs... \n");
	double abs = komplex_abs(a);
	double trueAbs = sqrt(5.0);
	if(abs == trueAbs)
		printf("test of 'abs' passed\n");
	else
		printf("test of 'abs' failed\n");

	return 0;
}
