#include <stdio.h>
#include <math.h>
#include <complex.h>

int main(){

	printf("Exercise 1\n");

	double g = gamma(5.0);
	printf("Gamma(5) = %g\n",g);
	
	double b = j1(0.5);
	printf("J1(0.5) = %g\n",b);

	double complex s = csqrt(-2);
	printf("sqrt(-2) = %g + %gi\n",creal(s),cimag(s));

	double complex e = cexp(I);
	printf("exp(i) = %g + %gi\n",creal(e),cimag(e));

	double complex p = cexp(I*M_PI);
	printf("exp(i*pi) = %g + %gi\n",creal(p),cimag(p));

	double complex j = cpow(I,exp(1));
	printf("i^e = %g + %gi\n",creal(j),cimag(j));


	printf("Exercise 2\n");

	float n1 = 0.1111111111111111111111111111;
	double n2 = 0.1111111111111111111111111111;
    	long double n3 = 0.1111111111111111111111111111L;
	printf("Testing significant digits:\n");
	printf("Float number = %.25g\n",n1);
	printf("Double number = %.25lg\n",n2);
	printf("Long double number = %.25Lg\n",n3);


	return 0;
}
